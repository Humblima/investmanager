﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Business.Models
{
    [Table("tblTempCryptoCotacao")]
    public class TblTempCryptoCotacao
    {
        [Key]
        [Column("pkTempCryptoCotacao")]
        public int PkTempCryptoCotacao { set; get; }
        [Required]
        [MaxLength(15)]
        [Column("cdTempMoedaFrom")]
        public string CdTempMoedaFrom { set; get; }
        [Required]
        [MaxLength(15)]
        [Column("cdTempMoedaTo")]
        public string CdTempMoedaTo { set; get; }
        [Required]
        [Column("nrTempCotacaoTimeUnix")]
        public long NrTempCotacaoTimeUnix { set; get; }

        [Required]
        [Column("dtTempCotacao")]
        public DateTime DtTempCotacao { set; get; }

        [Required]
        [Column("vlTempHigh", TypeName = "numeric(20,6)")]
        public double VlTempHigh { get; set; }
        [Required]
        [Column("vlTempLow",TypeName = "numeric(20,6)")]
        public double VlTempLow { get; set; }
        [Required]
        [Column("vlTempOpen",TypeName = "numeric(20,6)")]
        public double VlTempOpen { get; set; }

        [Required]
        [Column("vlTempClose", TypeName = "numeric(20,6)")]
        public double VlTempClose { get; set; }

        [Required]
        [MaxLength(500)]
        [Column("dsTempCotacaoConversaoTipo")]
        public string DsTempCotacaoConversaoTipo { set; get; }

        [Required]
        [MaxLength(500)]
        [Column("cdTempCotacaoConversaoFrom")]
        public string CdTempCotacaoConversaoFrom { set; get; }

        [Required]
        [MaxLength(100)]
        [Column("cdTempCotacaoArquivoHash")]
        public string CdCotacaoArquivoHash { set; get; }

    }



}
