﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Business.Models
{
    [Table("tblCryptoPar")]
    public class TblCryptoPar
    {
        [Key]
        public int pkCryptoPar { get; set; }

        [Required]
        [MaxLength(200)]
        public string nmCryptoParExchange { get; set; }

        [Required]
        [MaxLength(15)]
        public string cdCryptoParExchangeFrom { get; set; }

        [Required]
        [MaxLength(15)]
        public string cdCryptoParExchangeTo { get; set; }

        [Required]
        [MaxLength(15)]
        public string cdCryptoParFrom { get; set; }

        [Required]
        [MaxLength(15)]
        public string cdCryptoParTo { get; set; }

        public static async Task AtualizarParAsync(string hashArquivo, BdInvestManagerContext context)
        {
            List<VwExchangeParAtualizar> lstPares = context.VwExchangeParAtualizar.FromSqlRaw(" EXEC [dbo].[spRetornarExchangeParAtualizar] {0}", hashArquivo).ToList();
            int batchSize = 10;
            int batchCount = (int)Math.Ceiling((double)lstPares.Count() / batchSize);
            for (int i = 0; i < batchCount; i++)
            {
                var filesToUpload = lstPares.Skip(i * batchSize).Take(batchSize);
                var tasks = filesToUpload.Select(f => TblCryptoPar.AtualizarParExchange(f.NmExchange, hashArquivo, context));
                await Task.WhenAll(tasks);
            }
        }

        public static async Task AtualizarParExchange(string nmExchange, string hashArquivo, BdInvestManagerContext context)
        {
            string URL = "https://min-api.cryptocompare.com/data/v2/pair/mapping/exchange";
            string urlParameters = "?e=" + nmExchange;
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Apikey", "18100f167d273e45ab83459f9aebb729f6b83bfc70d219b88f17ef31826feb8d");

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.

            if (response.IsSuccessStatusCode)
            {
                // Parse the response body.
                var dataObjects = response.Content.ReadAsAsync<dynamic>().Result;  //Make sure to add a reference to System.Net.Http.Formatting.dll

                foreach (var item in dataObjects.Data.current)
                {
                    TblTempCryptoPar exPar = new TblTempCryptoPar();

                    exPar.NmTempCryptoParExchange = item.exchange;
                    exPar.CdTempCryptoParExchangeFrom = item.exchange_fsym;
                    exPar.CdTempCryptoParExchangeTo = item.exchange_tsym;
                    exPar.CdTempCryptoParFrom = item.fsym;
                    exPar.CdTempCryptoParTo = item.tsym;
                    exPar.CdTempCryptoParHashArquivo = hashArquivo;

                    context.Add(exPar);
                }

                await context.SaveChangesAsync();

                var qntRowsAfected = context.Database.ExecuteSqlRaw("EXEC [dbo].[spInserirNovosParesExchange] {0}", hashArquivo);
              
            }
        }
    }
}
