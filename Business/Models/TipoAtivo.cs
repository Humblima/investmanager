﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Business.Models
{
    public class TipoAtivo
    {
        [Key]
        public int pkTipoAtivo { get; set; }

        [MaxLength(100)]
        [Required]
        public string dsTipoAtivo { get; set; }
    }
}
