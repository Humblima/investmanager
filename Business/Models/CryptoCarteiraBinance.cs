﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class CryptoCarteiraBinance
    {
        public string CdCryptoMoeda { get; set; }
        public decimal vlQtdDisponivel { get; set; }
        public decimal vlQtdBloqueada { get; set; }
        public long timeStamp { get; set; }
    }
}
