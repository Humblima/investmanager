﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Business.Models
{
    public class tblCryptoMoeda
    {
        [Key]
        public int idMoeda { get; set; }

        [Required]
        [MaxLength(15)]
        public string cdMoeda { get; set; }

        [Required]
        [MaxLength(15)]
        public string nmMoeda { get; set; }
    }
}
