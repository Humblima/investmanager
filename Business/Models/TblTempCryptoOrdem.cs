﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Business.Models
{
    [Table("tblTempCryptoOrdem")]
    public class TblTempCryptoOrdem
    {
        [Key]
        [Column("pkTempCryptoOrdem")]
        public int PkTempCryptoOrdem { get; set; }

        [Required]
        [Column("dtTempCryptoOrdem")]
        public DateTime DtTempCryptoOrdem { get; set; }

        [MaxLength(30)]
        [Required]
        [Column("cdTempCryptoOrdemPar")]
        public string CdTempCryptoOrdemPar { get; set; }

        [MaxLength(30)]
        [Required]
        [Column("dsTempCryptoOrdemLado")]
        public string DsTempCryptoOrdemLado { get; set; }

        [Required]
        [Column("vlTempCryptoOrdemPreco", TypeName = "numeric(20,8)")]
        public double VlTempCryptoOrdemPreco { get; set; }

        [Required]
        [Column("nrTempCryptoOrdemQuantidade", TypeName = "numeric(20,8)")]
        public double NrTempCryptoOrdemQuantidade { get; set; }

        [Required]
        [Column("vlTempCryptoOrdemTotal", TypeName = "numeric(20,8)")]
        public double VlTempCryptoOrdemTotal { get; set; }

        [Required]
        [Column("vlTempCryptoOrdemTaxa", TypeName = "numeric(20,8)")]
        public double VlTempCryptoOrdemTaxa { get; set; }

        [Required]
        [MaxLength(15)]
        [Column("cdTempCryptoOrdemTaxaMoeda")]
        public string CdTempCryptoOrdemTaxaMoeda { get; set; }

        [MaxLength(200)]
        [Column("cdTempCryptoOrdemHashArquivo")]
        public string CdTempCryptoOrdemHashArquivo { get; set; }

        [MaxLength(100)]
        [Column("nmTempCryptoOrdemExchange")]
        public string NmTempCryptoOrdemExchange { get; set; }


        public static async Task<string> ImportarOrdensText(string conteudoARquivo, string nmExchange, BdInvestManagerContext context)
        {
            string[] linhas = new string[0];
            //byte[] data = Encoding.UTF8.GetBytes(conteudoARquivo);
            //SHA512 shaM = new SHA512Managed();
            //string hashArquivo = Encoding.UTF8.GetString(shaM.ComputeHash(data));
            string hashArquivo = conteudoARquivo.GetHashCode().ToString();

            if (context.TblCryptoOrdem.Where(p => p.HashArquivo == hashArquivo).Count() == 0)
            {
                if (conteudoARquivo.Contains("\r\n"))
                {
                    linhas = conteudoARquivo.Split("\r\n");
                }
                else if (conteudoARquivo.Contains("\n"))
                {
                    linhas = conteudoARquivo.Split("\n");
                }

                string[] dadosLinha = null;
                string[] cabecalho = null;
                DateTime minDataOrdem = DateTime.Now;
                DateTime maxDataOrdem = DateTime.Now;
                int nrLinha = 0;

                foreach (string linha in linhas)
                {

                    dadosLinha = linha.Trim().Split(";");
                    if (linha != null && linha.Trim().Length > 0)
                    {
                        nrLinha++;
                        if (nrLinha == 1)
                        {
                            cabecalho = linha.Split(";");
                        }
                        else
                        {

                            TblTempCryptoOrdem ordem = new TblTempCryptoOrdem
                            {
                                CdTempCryptoOrdemHashArquivo = hashArquivo,
                                NmTempCryptoOrdemExchange = nmExchange ?? "Binance"
                            };
                            for (int i = 0; i < dadosLinha.Length; i++)
                            {
                                switch (cabecalho[i])
                                {
                                    case "Date(UTC)":
                                        ordem.DtTempCryptoOrdem = DateTime.ParseExact(dadosLinha[i].Trim(), "yyyy-MM-dd HH:mm:ss", new CultureInfo("pt-br"), DateTimeStyles.None);
                                        if (ordem.DtTempCryptoOrdem.CompareTo(minDataOrdem) < 0)
                                        {
                                            minDataOrdem = ordem.DtTempCryptoOrdem;
                                        }
                                        if (ordem.DtTempCryptoOrdem.CompareTo(maxDataOrdem) > 0)
                                        {
                                            maxDataOrdem = ordem.DtTempCryptoOrdem;
                                        }
                                        break;
                                    case "Market":
                                        ordem.CdTempCryptoOrdemPar = Regex.Replace(dadosLinha[i], "[^A-Za-z0-9]+", ""); 
                                        break;
                                    case "Type":
                                        ordem.DsTempCryptoOrdemLado = dadosLinha[i].Trim();
                                        break;
                                    case "Price":
                                        ordem.VlTempCryptoOrdemPreco = double.Parse(dadosLinha[i].Trim(), new CultureInfo("en-us"));
                                        break;
                                    case "Amount":
                                        ordem.NrTempCryptoOrdemQuantidade = double.Parse(dadosLinha[i].Trim(), new CultureInfo("en-us"));
                                        break;
                                    case "Total":
                                        ordem.VlTempCryptoOrdemTotal = double.Parse(dadosLinha[i].Trim(), new CultureInfo("en-us"));
                                        break;
                                    case "Fee":
                                        ordem.VlTempCryptoOrdemTaxa = double.Parse(dadosLinha[i].Trim(), new CultureInfo("en-us"));
                                        break;
                                    case "Fee Coin":
                                        ordem.CdTempCryptoOrdemTaxaMoeda = dadosLinha[i].Trim();
                                        break;
                                    default:
                                        break;
                                }
                            }

                            context.Add(ordem);
                        }
                    }
                }
                context.SaveChanges();

                await TblCryptoPar.AtualizarParAsync(hashArquivo, context);
                var qntRowsAfected = context.Database.ExecuteSqlRaw("EXEC spCarregarCryptoOrdem {0}", hashArquivo);

                var taskAtualizarcotacoes = tblCryptoCotacao.AtualizarCotacoes(hashArquivo, context);
                var taskCotacaoPtax =  tblCotacaoPtax.AtualizarCotacaoPTAX(minDataOrdem, DateTime.Now, context);

                Task.WaitAll(taskAtualizarcotacoes, taskCotacaoPtax);

                

            }
            return hashArquivo;
        }
    }
}
