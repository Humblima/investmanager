﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Business.Models
{
    [Table("tblTempCryptoPar")]
    public class TblTempCryptoPar
    {
        [Key]
        [Column("pkTempCryptoPar")]
        public int PkTempCryptoPar { get; set; }

        [Required]
        [MaxLength(200)]
        [Column("nmTempCryptoParExchange")]
        public string NmTempCryptoParExchange { get; set; }

        [Required]
        [MaxLength(15)]
        [Column("cdTempCryptoParExchangeFrom")]
        public string CdTempCryptoParExchangeFrom { get; set; }

        [Required]
        [MaxLength(15)]
        [Column("cdTempCryptoParExchangeTo")]
        public string CdTempCryptoParExchangeTo { get; set; }

        [Required]
        [MaxLength(15)]
        [Column("cdTempCryptoParFrom")]
        public string CdTempCryptoParFrom { get; set; }

        [Required]
        [MaxLength(15)]
        [Column("cdTempCryptoParTo")]
        public string CdTempCryptoParTo { get; set; }

        [Required]
        [MaxLength(100)]
        [Column("cdTempCryptoParHashArquivo")]
        public string CdTempCryptoParHashArquivo { get; set; }
    }
}
