﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;

namespace Business.Models
{
    public class CarteiraBinance
    {
       private string urlBase = "https://api.binance.com";
        private string apiSecret = "3ySw2g0BCgYMFyXe1q0VZvJjlYB6ZRIc6oHs4uj8opaFUEMoSfcdo1std9npL11z";
        private string apiKey = "xH6GtlHSbtX5lBu7Mb2FOi9tzuEhcvVfr6R95mWZLQDH9GRswz0xp9569qrNK05g";

        private long RetornarTimeStampServer()
        {
            
            long retorno = 0;
            string URL = urlBase + "/api/v3/time";
            
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(urlBase);


            // List data response.
            HttpResponseMessage response = client.GetAsync(URL).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.

            if (response.IsSuccessStatusCode)
            {
                // Parse the response body.
                var dataObjects = response.Content.ReadAsAsync<dynamic>().Result;  //Make sure to add a reference to System.Net.Http.Formatting.dll
                retorno = dataObjects.serverTime;
            }

            return retorno;
        }
        private long GetTimestamp()
        {
            return new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();
        }

        private string CreateSignature(string queryString, string secret)
        {

            byte[] keyBytes = Encoding.UTF8.GetBytes(secret);
            byte[] queryStringBytes = Encoding.UTF8.GetBytes(queryString);
            HMACSHA256 hmacsha256 = new HMACSHA256(keyBytes);

            byte[] bytes = hmacsha256.ComputeHash(queryStringBytes);

            return BitConverter.ToString(bytes).Replace("-", "").ToLower();
        }

        public List<CryptoCarteiraBinance> RetornarPosicaoCarteira()
        {
            List<CryptoCarteiraBinance> lstcarteira = new List<CryptoCarteiraBinance>();
            string URL = urlBase + "/sapi/v1/capital/config/getall";
            long timestamp = this.GetTimestamp();

            string parameters = "timestamp=" + timestamp.ToString();
            string signature = this.CreateSignature(parameters, this.apiSecret);

            string urlParameters = "?timestamp=" + timestamp.ToString() + "&signature=" + signature;
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);
                        

            client.DefaultRequestHeaders.Add("X-MBX-APIKEY", apiKey);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.

            if (response.IsSuccessStatusCode)
            {
                // Parse the response body.
                var dataObjects = response.Content.ReadAsAsync<dynamic>().Result;  //Make sure to add a reference to System.Net.Http.Formatting.dll
                foreach (var item in dataObjects)
                {
                   
                        CryptoCarteiraBinance carteira = new CryptoCarteiraBinance();
                        carteira.CdCryptoMoeda = item.coin;
                        carteira.vlQtdDisponivel = item.free;
                        carteira.vlQtdBloqueada = item.locked;
                        lstcarteira.Add(carteira);
                }
            }

            return lstcarteira;
        }
    }
}
