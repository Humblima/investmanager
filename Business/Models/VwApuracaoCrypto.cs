﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class VwApuracaoCrypto
    {
        public DateTime dtCryptoOrdem { get; set; }
        public string cdCryptoMoeda { get; set; }
        public string TipoOrdem { get; set; }
        public DateTime dtCotacaoPtax { get; set; }
        public decimal precoUSD { get; set; }
        public decimal CotacaoDolar { get; set; }
        public decimal quantidade { get; set; }
        public decimal totalUSD { get; set; }
        public decimal corretagemUSD { get; set; }
        public decimal totalBRL { get; set; }
        public decimal corretagemBRL { get; set; }
        public string cdCryptoOrdemHashArquivo { get; set; }

    }
}
