﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Business.Models
{
    [Table("tblCryptoExchange")]
    public class TblCryptoExchange
    {
        [Key]
        [Column("pkCryptoExchange")]
        public int PkCryptoExchange { get; set; }

        [Column("idCryptoExchange")]
        public int IdCryptoExchange { get; set; }

        [Column("nmCryptoExchange")]
        [MaxLength(100)]
        public string NmCryptoExchange { get; set; }

        [Column("cdCryptoExchange")]
        [MaxLength(100)]
        public string CdCryptoExchange { get; set; }

        [Column("dsCryptoExchangeImagem")]
        [MaxLength(100)]
        public string DsCryptoExchangeImagem { get; set; }

        public static int AtualizarListaExchange(BdInvestManagerContext context)
        {
            string URL = "https://min-api.cryptocompare.com/data/";
            string urlParameters = "exchanges/general";
            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri(URL)
            };
            int result = 0;
            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.

            if (response.IsSuccessStatusCode)
            {
                // Parse the response body.
                var dataObjects = response.Content.ReadAsAsync<dynamic>().Result;  //Make sure to add a reference to System.Net.Http.Formatting.dll
                List<TblCryptoExchange> lstPar = new List<TblCryptoExchange>();
                foreach (var item in dataObjects.Data)
                {
                    TblCryptoExchange exPar = new TblCryptoExchange
                    {
                        IdCryptoExchange = item.Value.Id,
                        NmCryptoExchange = item.Value.Name,
                        CdCryptoExchange = item.Value.InternalName,
                        DsCryptoExchangeImagem = @"https://www.cryptocompare.com" + item.Value.LogoUrl
                    };

                    lstPar.Add(exPar);
                }
                var lstInternalizar = (from novo in lstPar
                                       join pars in context.TblCryptoExchanges on new { novo.CdCryptoExchange }
                                                                     equals new { pars.CdCryptoExchange}
                                       into inter
                                       where inter.Count() == 0
                                       select novo).ToList();
                
                context.TblCryptoExchanges.AddRange(lstInternalizar);

                context.SaveChanges();

               result = lstInternalizar.Count();
            }
            return result;
        }
    }
}
