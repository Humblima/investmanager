﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Business.Models
{
    [Table("tblCryptoOrdem")]
    public class TblCryptoOrdem
    {
        [Key]
        [Column("pkCryptoOrdem")]
        public int PkCryptoOrdem { get; set; }

        [Required]
        [Column("dtCryptoOrdem")]
        public DateTime DtCryptoOrdem { get; set; }

        [MaxLength(30)]
        [Required]
        [Column("cdCryptoOrdemPar")]
        public string CdCryptoOrdemPar { get; set; }

        [Required]
        [MaxLength(15)]
        [Column("cdCryptoOrdemMoedaCompra")]
        public string CdCryptoOrdemMoedaCompra { get; set; }

        [Required]
        [MaxLength(15)]
        [Column("cdCryptoOrdemMoedaVenda")]
        public string CdCryptoOrdemMoedaVenda { get; set; }

        [MaxLength(30)]
        [Required]
        [Column("dsCryptoOrdemLado")]
        public string DsCryptoOrdemLado { get; set; }

        [Required]
        [Column("vlCryptoOrdemPreco",TypeName = "numeric(20,8)")]
        public double VlCryptoOrdemPreco { get; set; }

        [Required]
        [Column("nrCryptoOrdemQuantidade",TypeName = "numeric(20,8)")]
        public double NrCryptoOrdemQuantidade { get; set; }

        [Required]
        [Column("vlCryptoOrdemTotal",TypeName = "numeric(20,8)")]
        public double VlCryptoOrdemTotal { get; set; }

        [Required]
        [Column("vlCryptoOrdemTaxa",TypeName = "numeric(20,8)")]
        public double VlCryptoOrdemTaxa { get; set; }

        [Required]
        [MaxLength(15)]
        [Column("cdCryptoOrdemTaxaMoeda")]
        public string CdCryptoOrdemTaxaMoeda { get; set; }

        [MaxLength(200)]
        [Column("cdCryptoOrdemHashArquivo")]
        public string HashArquivo { get; set; }

        [MaxLength(100)]
        [Column("nmCryptoOrdemExchange")]
        public string NmCryptoOrdemExchange { get; set; }
    }
}
