﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Business.Models
{
    public class Ativo
    {
        [Key]
        public int pkAtivo { get; set; }
        [MaxLength(20)]
        [Required]
        public string cdTicker { get; set; }
        [MaxLength(100)]
        [Required]
        public string nmAtivo { get; set; }
        [ForeignKey("fkTipoAtivo")]
        public TipoAtivo TipoAtivo { get; set; }

    }
}
