﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Business.Models
{
    
    public class VwCotacoesDesatulizadas
    {
        [Key]
        public string cdMoeda { get; set; }
        public DateTime dtMinOrdens { get; set; }
        public DateTime dtMaxOrdens { get; set; }
    }
}
