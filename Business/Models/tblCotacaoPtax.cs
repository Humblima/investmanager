﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Business.Models
{
    public class tblCotacaoPtax
    {
        [Key]
        public int PkCotacaoPtax { get; set; }

        [Required]
        public DateTime DtCotacaoPtax { get; set; }

        [Required]
        [Column(TypeName ="numeric(20,6)")]
        public double VlCotacaoPtaxCompra { get; set; }

        [Required]
        [Column(TypeName = "numeric(20,6)")]
        public double VlCotacaoPtaxVenda { get; set; }

        public static async Task AtualizarCotacaoPTAX(DateTime dtInicioIntervalo, DateTime dtFimIntervalo, BdInvestManagerContext context)
        {
            DateTime dtAnteriorInicio;

            try
            {
                dtAnteriorInicio = context.TblCotacaoPtax.Where(dt => dt.DtCotacaoPtax.CompareTo(dtInicioIntervalo) <= 0).Max(d => d.DtCotacaoPtax);
            }
            catch (Exception)
            {
                dtAnteriorInicio = DateTime.Now.AddMonths(-6);
            }
            
            
            string URL = "https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/odata/CotacaoDolarPeriodo(dataInicial=@dataInicial,dataFinalCotacao=@dataFinalCotacao)";
            string urlParameters = "?@dataInicial='" + dtInicioIntervalo.AddDays(-10).ToString("MM-dd-yyyy") + "'&@dataFinalCotacao='" + dtFimIntervalo.ToString("MM-dd-yyyy") + "'&$top=2000&$format=json";
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.

            if (response.IsSuccessStatusCode)
            {
                // Parse the response body.
                var dataObjects = response.Content.ReadAsAsync<dynamic>().Result;  //Make sure to add a reference to System.Net.Http.Formatting.dll

                List<tblCotacaoPtax> lstDolar = new List<tblCotacaoPtax>();
               
                    foreach (var item in dataObjects.value)
                    {

                        tblCotacaoPtax ptax = new tblCotacaoPtax();

                        ptax.DtCotacaoPtax = item.dataHoraCotacao;
                        ptax.VlCotacaoPtaxCompra = item.cotacaoCompra;
                        ptax.VlCotacaoPtaxVenda = item.cotacaoVenda;

                        lstDolar.Add(ptax);
                        
                    }
                var lstInportado = context.TblCotacaoPtax.Where(p => lstDolar.Select(d=> d.DtCotacaoPtax).Contains(p.DtCotacaoPtax)).ToList();
                lstDolar.RemoveAll(d=> lstInportado.Select(i => i.DtCotacaoPtax).Contains(d.DtCotacaoPtax));

                context.TblCotacaoPtax.AddRange(lstDolar);
                
                await context.SaveChangesAsync();
                
            }
        }
    }
}
