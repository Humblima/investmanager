﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Business.Models
{
    public class tblCryptoCotacao
    {
        [Key]
        public int PkCotacao { set; get; }
        [Required]
        [MaxLength(15)]
        public string CdMoedaFrom { set; get; }
        [Required]
        [MaxLength(15)]
        public string CdMoedaTo { set; get; }
        [Required]
        public long NrCotacaoTimeUnix { set; get; }

        [Required]
        public DateTime DtCotacao { set; get; }

        [Required]
        [Column(TypeName = "numeric(20,6)")]
        public double VlHigh { get; set; }
        [Required]
        [Column(TypeName = "numeric(20,6)")]
        public double VlLow { get; set; }
        [Required]
        [Column(TypeName = "numeric(20,6)")]
        public double VlOpen { get; set; }

        [Required]
        [Column(TypeName = "numeric(20,6)")]
        public double VlClose { get; set; }

        [Required]
        [MaxLength(500)]
        public string DsCotacaoConversaoTipo { set; get; }

        [Required]
        [MaxLength(500)]
        public string CdCotacaoConversaoFrom { set; get; }

        public static async Task AtualizarCotacoes(string hashArquivo, BdInvestManagerContext context)
        {

            List<VwCotacoesDesatulizadas> listAtualizarCotacao = context.VwCotacoesDesatulizadas.ToList();
            int batchSize = 10;
            int batchCount = (int)Math.Ceiling((double)listAtualizarCotacao.Count() / batchSize);
            for (int i = 0; i < batchCount; i++)
            {
                var filesToUpload = listAtualizarCotacao.Skip(i * batchSize).Take(batchSize);
                var tasks = filesToUpload.Select(f => tblCryptoCotacao.AtualizarBaseCotacao(f.dtMinOrdens, f.dtMaxOrdens, f.cdMoeda, "USD", hashArquivo, context));
                await Task.WhenAll(tasks);
            }
        }
        public static async Task AtualizarBaseCotacao(DateTime dtInicio, DateTime dtFim, string cdMoedaFrom, string cdMoedaTo, string hashArquivo, BdInvestManagerContext context)
        {

            int qntDias = (dtFim - dtInicio).Days +1;
            string URL = "https://min-api.cryptocompare.com/data/v2/histoday";
            string urlParameters = "?fsym=" + cdMoedaFrom + "&tsym=" + cdMoedaTo + "&limit=" + qntDias.ToString() + "&toTs=" + DateTimeOffset.FromFileTime(dtFim.ToFileTimeUtc()).ToUnixTimeSeconds().ToString();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.

            if (response.IsSuccessStatusCode)
            {
                // Parse the response body.
                var dataObjects = response.Content.ReadAsAsync<dynamic>().Result;  //Make sure to add a reference to System.Net.Http.Formatting.dll
                    foreach (var item in dataObjects.Data.Data)
                    {
                        Console.WriteLine("Data: {0}, Valor: {1}", DateTimeOffset.FromUnixTimeSeconds((long)item.time).LocalDateTime.ToString(), item.close.ToString("N2", new CultureInfo("pt-br")));

                        TblTempCryptoCotacao cotacao = new TblTempCryptoCotacao();
                        cotacao.CdTempMoedaFrom = cdMoedaFrom;
                        cotacao.CdTempMoedaTo = cdMoedaTo;
                        cotacao.NrTempCotacaoTimeUnix = (long)item.time;
                        cotacao.DtTempCotacao = DateTimeOffset.FromUnixTimeSeconds((long)item.time).UtcDateTime;
                        cotacao.VlTempHigh = (double)item.high;
                        cotacao.VlTempLow = (double)item.low;
                        cotacao.VlTempOpen = (double)item.open;
                        cotacao.VlTempClose = (double)item.close;
                        cotacao.DsTempCotacaoConversaoTipo = item.conversionType;
                        cotacao.CdTempCotacaoConversaoFrom = item.conversionSymbol;
                        cotacao.CdCotacaoArquivoHash = hashArquivo;
                        
                        context.Add(cotacao);
                    }
                await context.SaveChangesAsync();

                var qntRowsAfected = context.Database.ExecuteSqlRaw("EXEC spInserirCotacoesCrypto");
            }
            else
            {
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }

            //Make any other calls using HttpClient here.

            //Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
            client.Dispose();
        }
    }



}
