﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Business.Migrations
{
    public partial class AtualizacaoDataType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "VlOpen",
                table: "TblCryptoCotacao",
                type: "numeric(20,6)",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "float");

            migrationBuilder.AlterColumn<decimal>(
                name: "VlLow",
                table: "TblCryptoCotacao",
                type: "numeric(20,6)",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "float");

            migrationBuilder.AlterColumn<decimal>(
                name: "VlHigh",
                table: "TblCryptoCotacao",
                type: "numeric(20,6)",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "float");

            migrationBuilder.AlterColumn<decimal>(
                name: "VlClose",
                table: "TblCryptoCotacao",
                type: "numeric(20,6)",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "float");

            migrationBuilder.AlterColumn<decimal>(
                name: "VlCotacaoPtaxVenda",
                table: "TblCotacaoPtax",
                type: "numeric(20,6)",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "float");

            migrationBuilder.AlterColumn<decimal>(
                name: "VlCotacaoPtaxCompra",
                table: "TblCotacaoPtax",
                type: "numeric(20,6)",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "float");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "VlOpen",
                table: "TblCryptoCotacao",
                type: "float",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(20,6)");

            migrationBuilder.AlterColumn<double>(
                name: "VlLow",
                table: "TblCryptoCotacao",
                type: "float",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(20,6)");

            migrationBuilder.AlterColumn<double>(
                name: "VlHigh",
                table: "TblCryptoCotacao",
                type: "float",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(20,6)");

            migrationBuilder.AlterColumn<double>(
                name: "VlClose",
                table: "TblCryptoCotacao",
                type: "float",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(20,6)");

            migrationBuilder.AlterColumn<double>(
                name: "VlCotacaoPtaxVenda",
                table: "TblCotacaoPtax",
                type: "float",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(20,6)");

            migrationBuilder.AlterColumn<double>(
                name: "VlCotacaoPtaxCompra",
                table: "TblCotacaoPtax",
                type: "float",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(20,6)");
        }
    }
}
