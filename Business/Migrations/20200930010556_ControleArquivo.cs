﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Business.Migrations
{
    public partial class ControleArquivo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "vlCryptoOrdemTotal",
                table: "TblCryptoOrdem",
                type: "numeric(20,8)",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "float");

            migrationBuilder.AlterColumn<decimal>(
                name: "vlCryptoOrdemTaxa",
                table: "TblCryptoOrdem",
                type: "numeric(20,8)",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "float");

            migrationBuilder.AlterColumn<decimal>(
                name: "vlCryptoOrdemPreco",
                table: "TblCryptoOrdem",
                type: "numeric(20,8)",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "float");

            migrationBuilder.AlterColumn<decimal>(
                name: "nrCryptoOrdemQuantidade",
                table: "TblCryptoOrdem",
                type: "numeric(20,8)",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "float");

            migrationBuilder.AddColumn<string>(
                name: "cdCryptoOrdemHashArquivo",
                table: "TblCryptoOrdem",
                maxLength: 200,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "cdCryptoOrdemHashArquivo",
                table: "TblCryptoOrdem");

            migrationBuilder.AlterColumn<double>(
                name: "vlCryptoOrdemTotal",
                table: "TblCryptoOrdem",
                type: "float",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(20,8)");

            migrationBuilder.AlterColumn<double>(
                name: "vlCryptoOrdemTaxa",
                table: "TblCryptoOrdem",
                type: "float",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(20,8)");

            migrationBuilder.AlterColumn<double>(
                name: "vlCryptoOrdemPreco",
                table: "TblCryptoOrdem",
                type: "float",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(20,8)");

            migrationBuilder.AlterColumn<double>(
                name: "nrCryptoOrdemQuantidade",
                table: "TblCryptoOrdem",
                type: "float",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(20,8)");
        }
    }
}
