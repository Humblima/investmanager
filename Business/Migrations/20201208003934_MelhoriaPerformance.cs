﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Business.Migrations
{
    public partial class MelhoriaPerformance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_TblCryptoOrdem",
                table: "TblCryptoOrdem");

            migrationBuilder.RenameTable(
                name: "TblCryptoOrdem",
                newName: "tblCryptoOrdem");

            migrationBuilder.AddPrimaryKey(
                name: "PK_tblCryptoOrdem",
                table: "tblCryptoOrdem",
                column: "pkCryptoOrdem");

            migrationBuilder.CreateTable(
                name: "tblTempCryptoPar",
                columns: table => new
                {
                    pkTempCryptoPar = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nmTempCryptoParExchange = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    cdTempCryptoParExchangeFrom = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    cdTempCryptoParExchangeTo = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    cdTempCryptoParFrom = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    cdTempCryptoParTo = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    cdTempCryptoParHashArquivo = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblTempCryptoPar", x => x.pkTempCryptoPar);
                });
            string spRetornarExchangeAtualizar = @"CREATE PROCEDURE [dbo].[spRetornarExchangeParAtualizar]

    @cdHashArquivo varchar(200)
AS
BEGIN

    SELECT distinct

    t.nmTempCryptoOrdemExchange as nmExchange

    FROM tblTempCryptoOrdem as t

    left join tblCryptoPar as p on p.nmCryptoParExchange = t.nmTempCryptoOrdemExchange

                                AND CONCAT(P.cdCryptoParExchangeFrom, P.cdCryptoParExchangeTo) = T.cdTempCryptoOrdemPar
    WHERE P.pkCryptoPar is null
    and T.cdTempCryptoOrdemHashArquivo = @cdHashArquivo
END";
            string spInserirNovosParesExchange = @"CREATE PROCEDURE [dbo].[spInserirNovosParesExchange]
	@cdHashArquivo varchar(100)
AS
BEGIN
	INSERT INTO tblCryptoPar (
	nmCryptoParExchange, 
	cdCryptoParExchangeFrom,
	cdCryptoParExchangeTo,
	cdCryptoParFrom,
	cdCryptoParTo
	)
	SELECT 
	T.nmTempCryptoParExchange, 
	T.cdTempCryptoParExchangeFrom,
	T.cdTempCryptoParExchangeTo,
	T.cdTempCryptoParFrom,
	T.cdTempCryptoParTo
	FROM tblTempCryptoPar as T
	LEFT JOIN tblCryptoPar as P on P.nmCryptoParExchange = T.nmTempCryptoParExchange 
								AND P.cdCryptoParFrom = T.cdTempCryptoParFrom 
								AND P.cdCryptoParTo = T.cdTempCryptoParTo
	WHERE P.pkCryptoPar is null
	AND T.cdTempCryptoParHashArquivo = @cdHashArquivo

	DELETE T FROM tblTempCryptoPar as T
	WHERE T.cdTempCryptoParHashArquivo = @cdHashArquivo
END";
            migrationBuilder.Sql(spRetornarExchangeAtualizar);
            migrationBuilder.Sql(spInserirNovosParesExchange);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tblTempCryptoPar");

            migrationBuilder.DropPrimaryKey(
                name: "PK_tblCryptoOrdem",
                table: "tblCryptoOrdem");

            migrationBuilder.RenameTable(
                name: "tblCryptoOrdem",
                newName: "TblCryptoOrdem");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TblCryptoOrdem",
                table: "TblCryptoOrdem",
                column: "pkCryptoOrdem");
        }
    }
}
