﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Business.Migrations
{
    public partial class ApuracaoImpostoCryptos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TblCotacaoPtax",
                columns: table => new
                {
                    PkCotacaoPtax = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DtCotacaoPtax = table.Column<DateTime>(nullable: false),
                    VlCotacaoPtaxCompra = table.Column<double>(nullable: false),
                    VlCotacaoPtaxVenda = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblCotacaoPtax", x => x.PkCotacaoPtax);
                });

            migrationBuilder.CreateTable(
                name: "TblCryptoCotacao",
                columns: table => new
                {
                    PkCotacao = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CdMoedaFrom = table.Column<string>(maxLength: 15, nullable: false),
                    CdMoedaTo = table.Column<string>(maxLength: 15, nullable: false),
                    NrCotacaoTimeUnix = table.Column<long>(nullable: false),
                    DtCotacao = table.Column<DateTime>(nullable: false),
                    VlHigh = table.Column<decimal>(nullable: false),
                    VlLow = table.Column<decimal>(nullable: false),
                    VlOpen = table.Column<decimal>(nullable: false),
                    VlClose = table.Column<decimal>(nullable: false),
                    DsCotacaoConversaoTipo = table.Column<string>(maxLength: 500, nullable: false),
                    CdCotacaoConversaoFrom = table.Column<string>(maxLength: 500, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblCryptoCotacao", x => x.PkCotacao);
                });

            migrationBuilder.CreateTable(
                name: "TblCryptoMoeda",
                columns: table => new
                {
                    idMoeda = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    cdMoeda = table.Column<string>(maxLength: 15, nullable: false),
                    nmMoeda = table.Column<string>(maxLength: 15, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblCryptoMoeda", x => x.idMoeda);
                });

            migrationBuilder.CreateTable(
                name: "TblCryptoOrdem",
                columns: table => new
                {
                    pkCryptoOrdem = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    dtCryptoOrdem = table.Column<DateTime>(nullable: false),
                    cdCryptoOrdemPar = table.Column<string>(maxLength: 30, nullable: false),
                    cdCryptoOrdemMoedaCompra = table.Column<string>(maxLength: 15, nullable: false),
                    cdCryptoOrdemMoedaVenda = table.Column<string>(maxLength: 15, nullable: false),
                    dsCryptoOrdemLado = table.Column<string>(maxLength: 30, nullable: false),
                    vlCryptoOrdemPreco = table.Column<decimal>(nullable: false),
                    nrCryptoOrdemQuantidade = table.Column<decimal>(nullable: false),
                    vlCryptoOrdemTotal = table.Column<decimal>(nullable: false),
                    vlCryptoOrdemTaxa = table.Column<decimal>(nullable: false),
                    cdCryptoOrdemTaxaMoeda = table.Column<string>(maxLength: 15, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblCryptoOrdem", x => x.pkCryptoOrdem);
                });

            migrationBuilder.CreateTable(
                name: "TblCryptoPar",
                columns: table => new
                {
                    pkCryptoPar = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nmCryptoParExchange = table.Column<string>(maxLength: 200, nullable: false),
                    cdCryptoParExchangeFrom = table.Column<string>(maxLength: 15, nullable: false),
                    cdCryptoParExchangeTo = table.Column<string>(maxLength: 15, nullable: false),
                    cdCryptoParFrom = table.Column<string>(maxLength: 15, nullable: false),
                    cdCryptoParTo = table.Column<string>(maxLength: 15, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblCryptoPar", x => x.pkCryptoPar);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TblCotacaoPtax");

            migrationBuilder.DropTable(
                name: "TblCryptoCotacao");

            migrationBuilder.DropTable(
                name: "TblCryptoMoeda");

            migrationBuilder.DropTable(
                name: "TblCryptoOrdem");

            migrationBuilder.DropTable(
                name: "TblCryptoPar");
        }
    }
}
