﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Business.Migrations
{
    public partial class InvestManagerMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TblTipoAtivo",
                columns: table => new
                {
                    pkTipoAtivo = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    dsTipoAtivo = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblTipoAtivo", x => x.pkTipoAtivo);
                });

            migrationBuilder.CreateTable(
                name: "TblAtivos",
                columns: table => new
                {
                    pkAtivo = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    cdTicker = table.Column<string>(maxLength: 20, nullable: false),
                    nmAtivo = table.Column<string>(maxLength: 100, nullable: false),
                    fkTipoAtivo = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblAtivos", x => x.pkAtivo);
                    table.ForeignKey(
                        name: "FK_TblAtivos_TblTipoAtivo_fkTipoAtivo",
                        column: x => x.fkTipoAtivo,
                        principalTable: "TblTipoAtivo",
                        principalColumn: "pkTipoAtivo",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TblAtivos_fkTipoAtivo",
                table: "TblAtivos",
                column: "fkTipoAtivo");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TblAtivos");

            migrationBuilder.DropTable(
                name: "TblTipoAtivo");
        }
    }
}
