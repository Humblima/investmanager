﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Business.Migrations
{
    public partial class PerformanceCotacaoCrypto : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tblTempCryptoCotacao",
                columns: table => new
                {
                    pkTempCryptoCotacao = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    cdTempMoedaFrom = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    cdTempMoedaTo = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    nrTempCotacaoTimeUnix = table.Column<long>(type: "bigint", nullable: false),
                    dtTempCotacao = table.Column<DateTime>(type: "datetime2", nullable: false),
                    vlTempHigh = table.Column<decimal>(type: "numeric(20,6)", nullable: false),
                    vlTempLow = table.Column<decimal>(type: "numeric(20,6)", nullable: false),
                    vlTempOpen = table.Column<decimal>(type: "numeric(20,6)", nullable: false),
                    vlTempClose = table.Column<decimal>(type: "numeric(20,6)", nullable: false),
                    dsTempCotacaoConversaoTipo = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    cdTempCotacaoConversaoFrom = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    cdTempCotacaoArquivoHash = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblTempCryptoCotacao", x => x.pkTempCryptoCotacao);
                });
			string spInserirCotacoes = @"CREATE PROCEDURE [dbo].[spInserirCotacoesCrypto]
AS
BEGIN
	UPDATE C
	SET C.VlOpen = t.vlTempOpen
	, C.VlClose = t.vlTempClose
	, C.VlHigh = t.vlTempHigh
	, C.VlLow = t.vlTempLow
	FROM tblTempCryptoCotacao as T
	INNER join TblCryptoCotacao as c on c.CdMoedaFrom = t.cdTempMoedaFrom 
										AND C.CdMoedaTo = T.cdTempMoedaTo
										AND C.DtCotacao = t.dtTempCotacao
										AND (
											C.VlOpen <> t.vlTempOpen 
											OR C.VlHigh <> t.vlTempHigh
											OR C.VlClose <> t.vlTempClose
											OR C.VlLow <> t.vlTempLow 
											)

	INSERT INTO TblCryptoCotacao (
	CdMoedaFrom
	,CdMoedaTo
	,NrCotacaoTimeUnix
	,DtCotacao
	,VlHigh
	,VlLow
	,VlOpen
	,VlClose
	,DsCotacaoConversaoTipo
	,CdCotacaoConversaoFrom
	)
	SELECT 
	 T.cdTempMoedaFrom
	,T.cdTempMoedaTo
	,T.nrTempCotacaoTimeUnix
	,T.dtTempCotacao
	,T.vlTempHigh
	,T.vlTempLow
	,T.vlTempOpen
	,T.vlTempClose
	,T.dsTempCotacaoConversaoTipo
	,T.cdTempCotacaoConversaoFrom
	FROM tblTempCryptoCotacao as T
	LEFT join TblCryptoCotacao as c on c.CdMoedaFrom = t.cdTempMoedaFrom 
										AND C.CdMoedaTo = T.cdTempMoedaTo
										AND C.DtCotacao = t.dtTempCotacao
										AND C.VlOpen = t.vlTempOpen 
										AND C.VlHigh = t.vlTempHigh
										AND C.VlClose = t.vlTempClose
										AND C.VlLow = t.vlTempLow
	WHERE C.pkCotacao is null

	DELETE tblTempCryptoCotacao
											
END
";
			migrationBuilder.Sql(spInserirCotacoes);

		}

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tblTempCryptoCotacao");
        }
    }
}
