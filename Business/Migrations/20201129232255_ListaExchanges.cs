﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Business.Migrations
{
    public partial class ListaExchanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_TblCryptoPar",
                table: "TblCryptoPar");

            migrationBuilder.RenameTable(
                name: "TblCryptoPar",
                newName: "tblCryptoPar");

            migrationBuilder.AddColumn<string>(
                name: "nmCryptoOrdemExchange",
                table: "TblCryptoOrdem",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_tblCryptoPar",
                table: "tblCryptoPar",
                column: "pkCryptoPar");

            migrationBuilder.CreateTable(
                name: "tblCryptoExchange",
                columns: table => new
                {
                    pkCryptoExchange = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    idCryptoExchange = table.Column<int>(type: "int", nullable: false),
                    nmCryptoExchange = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    cdCryptoExchange = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    dsCryptoExchangeImagem = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblCryptoExchange", x => x.pkCryptoExchange);
                });

            migrationBuilder.CreateTable(
                name: "tblTempCryptoOrdem",
                columns: table => new
                {
                    pkTempCryptoOrdem = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    dtTempCryptoOrdem = table.Column<DateTime>(type: "datetime2", nullable: false),
                    cdTempCryptoOrdemPar = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    dsTempCryptoOrdemLado = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    vlTempCryptoOrdemPreco = table.Column<decimal>(type: "numeric(20,8)", nullable: false),
                    nrTempCryptoOrdemQuantidade = table.Column<decimal>(type: "numeric(20,8)", nullable: false),
                    vlTempCryptoOrdemTotal = table.Column<decimal>(type: "numeric(20,8)", nullable: false),
                    vlTempCryptoOrdemTaxa = table.Column<decimal>(type: "numeric(20,8)", nullable: false),
                    cdTempCryptoOrdemTaxaMoeda = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    cdTempCryptoOrdemHashArquivo = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    nmTempCryptoOrdemExchange = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblTempCryptoOrdem", x => x.pkTempCryptoOrdem);
                });
            string view = @"CREATE VIEW [dbo].[vwExchangeParAtualizar]
	AS 
	SELECT distinct
	t.nmTempCryptoOrdemExchange  as nmExchange
	FROM tblTempCryptoOrdem as t 
	left join tblCryptoPar as p on p.nmCryptoParExchange = t.nmTempCryptoOrdemExchange 
								AND CONCAT(P.cdCryptoParExchangeFrom, P.cdCryptoParExchangeTo) = T.cdTempCryptoOrdemPar 
    WHERE P.pkCryptoPar is null ";

            string spCarregarOrdem = @"CREATE PROCEDURE [dbo].[spCarregarCryptoOrdem]
	@HashArquivoOrdens varchar(200)
AS
BEGIN

	INSERT INTO TblCryptoOrdem 
	(dtCryptoOrdem
	,cdCryptoOrdemPar
	,cdCryptoOrdemMoedaCompra
	,cdCryptoOrdemMoedaVenda
	,dsCryptoOrdemLado
	,vlCryptoOrdemPreco
	,nrCryptoOrdemQuantidade
	,vlCryptoOrdemTotal
	,vlCryptoOrdemTaxa
	,cdCryptoOrdemTaxaMoeda
	,cdCryptoOrdemHashArquivo
	,nmCryptoOrdemExchange
	)
	SELECT 
	 dtTempCryptoOrdem
	,cdTempCryptoOrdemPar
	,cdCryptoOrdemMoedaCompra = P.cdCryptoParExchangeFrom
	,cdCryptoOrdemMoedaVenda = P.cdCryptoParExchangeTo
	,dsTempCryptoOrdemLado
	,vlTempCryptoOrdemPreco
	,nrTempCryptoOrdemQuantidade
	,vlTempCryptoOrdemTotal
	,vlTempCryptoOrdemTaxa
	,cdTempCryptoOrdemTaxaMoeda
	,cdTempCryptoOrdemHashArquivo
	,nmTempCryptoOrdemExchange
	FROM TblTempCryptoOrdem as t
	left join tblCryptoPar as p on p.nmCryptoParExchange = t.nmTempCryptoOrdemExchange 
								AND CONCAT(P.cdCryptoParExchangeFrom, P.cdCryptoParExchangeTo) = T.cdTempCryptoOrdemPar
	where T.cdTempCryptoOrdemHashArquivo = @HashArquivoOrdens	
END";
            string spRemoverArquivo = @"CREATE PROCEDURE [dbo].[spExcluirArquivoOrdens]

    @dsHashArquivo varchar(200)
AS
    DELETE tblTempCryptoOrdem WHERE cdTempCryptoOrdemHashArquivo = @dsHashArquivo

    DELETE TblCryptoOrdem WHERE cdCryptoOrdemHashArquivo = @dsHashArquivo";
            string alterVwApuracao = @"ALTER VIEW [dbo].[vwApuracao]
	AS 
	 SELECT 
O.pkCryptoOrdem
,O.dtCryptoOrdem
,O.cdCryptoOrdemMoedaCompra as cdCryptoMoeda
, CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN  'Compra' 
		WHEN O.dsCryptoOrdemLado = 'SELL' THEN 'Venda' END AS  TipoOrdem
, precoUSD =CASE WHEN o.cdCryptoOrdemMoedaVenda = 'USD' THEN  O.vlCryptoOrdemPreco
				 WHEN o.cdCryptoOrdemMoedaVenda = 'BRL' THEN  CONVERT(numeric(19,6),O.vlCryptoOrdemPreco/CASe WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxCompra END)
				 ELSE CC.VlClose
				 END 
, PT.DtCotacaoPtax
, CotacaoDolar = CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxCompra END
, Quantidade = O.nrCryptoOrdemQuantidade
, totalUSD = CASE WHEN o.cdCryptoOrdemMoedaVenda = 'USD' THEN  O.vlCryptoOrdemTotal
				 WHEN o.cdCryptoOrdemMoedaVenda = 'BRL' THEN CONVERT(numeric(19,6),O.vlCryptoOrdemTotal/CASe WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxCompra END)
				 ELSE CONVERT(numeric(19,6),CC.VlClose * O.nrCryptoOrdemQuantidade)
				 END
, corretagemUSD = CASE WHEN o.cdCryptoOrdemMoedaVenda = 'USD' THEN  o.vlCryptoOrdemTaxa
					   WHEN o.cdCryptoOrdemMoedaVenda = 'BRL' THEN CONVERT(numeric(19,6),o.vlCryptoOrdemTaxa/CASe WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxCompra END)
					   ELSE CONVERT(numeric(19,6),ct.VlClose * o.vlCryptoOrdemTaxa/2) 
				  END
, TotalBRL =CASE WHEN o.cdCryptoOrdemMoedaVenda = 'USD' THEN CONVERT(numeric(19,6), o.vlCryptoOrdemTaxa * CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxCompra END)
					   WHEN o.cdCryptoOrdemMoedaVenda = 'BRL' THEN  O.vlCryptoOrdemTotal
					   ELSE CONVERT(numeric(19,6),CC.VlClose * O.nrCryptoOrdemQuantidade * CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxCompra END) 
					END
, corretagemBRL = CASE WHEN o.cdCryptoOrdemMoedaVenda = 'USD' THEN CONVERT(numeric(19,6),o.vlCryptoOrdemTaxa * CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxCompra END)
						WHEN o.cdCryptoOrdemMoedaVenda = 'BRL' THEN o.vlCryptoOrdemTaxa
						ELSE CONVERT(numeric(19,6),(ct.VlClose * o.vlCryptoOrdemTaxa/2) * CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxCompra END)
					END
, O.cdCryptoOrdemHashArquivo
from TblCryptoOrdem as O 
left join TblCryptoCotacao as CC on Cc.CdMoedaFrom = o.cdCryptoOrdemMoedaCompra and CONVERT(date, cC.DtCotacao) = CONVERT(date, O.dtCryptoOrdem)
left join TblCryptoCotacao as ct on ct.CdMoedaFrom = o.cdCryptoOrdemTaxaMoeda and CONVERT(date, ct.DtCotacao) = CONVERT(date, O.dtCryptoOrdem)
left join TblCotacaoPtax as PT on CONVERT(date, PT.DtCotacaoPtax) = CONVERT(date, dbo.fcRetornarDataPtax(O.dtCryptoOrdem))

UNION ALL

SELECT 
O.pkCryptoOrdem
,O.dtCryptoOrdem
,O.cdCryptoOrdemMoedaVenda as cdCryptoMoeda
, CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN 'Venda' 
		WHEN O.dsCryptoOrdemLado = 'SELL' THEN 'Compra'  END AS  TipoOrdem
, preco = CV.VlClose
, PT.DtCotacaoPtax
, CotacaoDolar = CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxCompra WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxVenda END 
, Quantidade = O.vlCryptoOrdemTotal
, total = CONVERT(numeric(19,6),CV.VlClose * O.vlCryptoOrdemTotal)
, corretagem = CONVERT(numeric(19,6),ct.VlClose * o.vlCryptoOrdemTaxa/2)
, TotalBRL = CONVERT(numeric(19,6),CV.VlClose * O.vlCryptoOrdemTotal * CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxCompra WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxVenda END )
, corretagemBRL = CONVERT(numeric(19,6),(ct.VlClose * o.vlCryptoOrdemTaxa/2) * CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxCompra WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxVenda END )
, O.cdCryptoOrdemHashArquivo
from TblCryptoOrdem as O 
left join TblCryptoCotacao as CV on CV.CdMoedaFrom = o.cdCryptoOrdemMoedaVenda and CONVERT(date, cV.DtCotacao) = CONVERT(date, O.dtCryptoOrdem)
left join TblCryptoCotacao as ct on ct.CdMoedaFrom = o.cdCryptoOrdemTaxaMoeda and CONVERT(date, ct.DtCotacao) = CONVERT(date, O.dtCryptoOrdem)
left join TblCotacaoPtax as PT on CONVERT(date, PT.DtCotacaoPtax) = CONVERT(date, dbo.fcRetornarDataPtax(O.dtCryptoOrdem))
WHERE O.cdCryptoOrdemMoedaVenda not in ('USD' , 'BRL')

UNION ALL

 SELECT 
O.pkCryptoOrdem
,O.dtCryptoOrdem
,O.cdCryptoOrdemTaxaMoeda as cdCryptoMoeda
, 'Venda - taxa'  AS  TipoOrdem
, preco = Ct.VlClose
, PT.DtCotacaoPtax
, CotacaoDolar = pt.VlCotacaoPtaxCompra
, Quantidade = O.vlCryptoOrdemTaxa
, total = CONVERT(numeric(19,6),Ct.VlClose * O.vlCryptoOrdemTaxa)
, corretagem = 0.000000
, totalBrl = CONVERT(numeric(19,6),Ct.VlClose * O.vlCryptoOrdemTaxa) * pt.VlCotacaoPtaxCompra
, corretagemBrl = 0.000000
, O.cdCryptoOrdemHashArquivo
from TblCryptoOrdem as O 
left join TblCryptoCotacao as ct on ct.CdMoedaFrom = o.cdCryptoOrdemTaxaMoeda and CONVERT(date, ct.DtCotacao) = CONVERT(date, O.dtCryptoOrdem)
left join TblCotacaoPtax as PT on CONVERT(date, PT.DtCotacaoPtax) = CONVERT(date, dbo.fcRetornarDataPtax(O.dtCryptoOrdem))
WHERE O.cdCryptoOrdemMoedaVenda not in ('USD' , 'BRL')";

			string spRetornarApuracao = @"CREATE PROCEDURE [dbo].[spRetornarApuracao]
	@HashArquivo varchar (200)
	
AS
BEGIN 

	SELECT 
	O.pkCryptoOrdem
	,O.dtCryptoOrdem
	,O.cdCryptoOrdemMoedaCompra as cdCryptoMoeda
	, CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN  'Compra' 
			WHEN O.dsCryptoOrdemLado = 'SELL' THEN 'Venda' END AS  TipoOrdem
	, precoUSD =CASE WHEN o.cdCryptoOrdemMoedaVenda = 'USD' THEN  O.vlCryptoOrdemPreco
					 WHEN o.cdCryptoOrdemMoedaVenda = 'BRL' THEN  CONVERT(numeric(19,6),O.vlCryptoOrdemPreco/CASe WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxCompra END)
					 ELSE CC.VlClose
					 END 
	, PT.DtCotacaoPtax
	, CotacaoDolar = CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxCompra END
	, Quantidade = O.nrCryptoOrdemQuantidade
	, totalUSD = CASE WHEN o.cdCryptoOrdemMoedaVenda = 'USD' THEN  O.vlCryptoOrdemTotal
					 WHEN o.cdCryptoOrdemMoedaVenda = 'BRL' THEN CONVERT(numeric(19,6),O.vlCryptoOrdemTotal/CASe WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxCompra END)
					 ELSE CONVERT(numeric(19,6),CC.VlClose * O.nrCryptoOrdemQuantidade)
					 END
	, corretagemUSD = CASE WHEN o.cdCryptoOrdemMoedaVenda = 'USD' THEN  o.vlCryptoOrdemTaxa
						   WHEN o.cdCryptoOrdemMoedaVenda = 'BRL' THEN CONVERT(numeric(19,6),o.vlCryptoOrdemTaxa/CASe WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxCompra END)
						   ELSE CONVERT(numeric(19,6),ct.VlClose * o.vlCryptoOrdemTaxa/2) 
					  END
	, TotalBRL =CASE WHEN o.cdCryptoOrdemMoedaVenda = 'USD' THEN CONVERT(numeric(19,6), o.vlCryptoOrdemTaxa * CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxCompra END)
						   WHEN o.cdCryptoOrdemMoedaVenda = 'BRL' THEN  O.vlCryptoOrdemTotal
						   ELSE CONVERT(numeric(19,6),CC.VlClose * O.nrCryptoOrdemQuantidade * CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxCompra END) 
						END
	, corretagemBRL = CASE WHEN o.cdCryptoOrdemMoedaVenda = 'USD' THEN CONVERT(numeric(19,6),o.vlCryptoOrdemTaxa * CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxCompra END)
							WHEN o.cdCryptoOrdemMoedaVenda = 'BRL' THEN o.vlCryptoOrdemTaxa
							ELSE CONVERT(numeric(19,6),(ct.VlClose * o.vlCryptoOrdemTaxa/2) * CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxCompra END)
						END
	, O.cdCryptoOrdemHashArquivo
	from TblCryptoOrdem as O 
	left join TblCryptoCotacao as CC on Cc.CdMoedaFrom = o.cdCryptoOrdemMoedaCompra and CONVERT(date, cC.DtCotacao) = CONVERT(date, O.dtCryptoOrdem)
	left join TblCryptoCotacao as ct on ct.CdMoedaFrom = o.cdCryptoOrdemTaxaMoeda and CONVERT(date, ct.DtCotacao) = CONVERT(date, O.dtCryptoOrdem)
	left join TblCotacaoPtax as PT on CONVERT(date, PT.DtCotacaoPtax) = CONVERT(date, dbo.fcRetornarDataPtax(O.dtCryptoOrdem))
	where  O.cdCryptoOrdemHashArquivo = @HashArquivo

	UNION ALL

	SELECT 
	O.pkCryptoOrdem
	,O.dtCryptoOrdem
	,O.cdCryptoOrdemMoedaVenda as cdCryptoMoeda
	, CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN 'Venda' 
			WHEN O.dsCryptoOrdemLado = 'SELL' THEN 'Compra'  END AS  TipoOrdem
	, preco = CV.VlClose
	, PT.DtCotacaoPtax
	, CotacaoDolar = CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxCompra WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxVenda END 
	, Quantidade = O.vlCryptoOrdemTotal
	, total = CONVERT(numeric(19,6),CV.VlClose * O.vlCryptoOrdemTotal)
	, corretagem = CONVERT(numeric(19,6),ct.VlClose * o.vlCryptoOrdemTaxa/2)
	, TotalBRL = CONVERT(numeric(19,6),CV.VlClose * O.vlCryptoOrdemTotal * CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxCompra WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxVenda END )
	, corretagemBRL = CONVERT(numeric(19,6),(ct.VlClose * o.vlCryptoOrdemTaxa/2) * CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxCompra WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxVenda END )
	, O.cdCryptoOrdemHashArquivo
	from TblCryptoOrdem as O 
	left join TblCryptoCotacao as CV on CV.CdMoedaFrom = o.cdCryptoOrdemMoedaVenda and CONVERT(date, cV.DtCotacao) = CONVERT(date, O.dtCryptoOrdem)
	left join TblCryptoCotacao as ct on ct.CdMoedaFrom = o.cdCryptoOrdemTaxaMoeda and CONVERT(date, ct.DtCotacao) = CONVERT(date, O.dtCryptoOrdem)
	left join TblCotacaoPtax as PT on CONVERT(date, PT.DtCotacaoPtax) = CONVERT(date, dbo.fcRetornarDataPtax(O.dtCryptoOrdem))
	WHERE O.cdCryptoOrdemMoedaVenda not in ('USD' , 'BRL')
	AND O.cdCryptoOrdemHashArquivo = @HashArquivo

	UNION ALL

	 SELECT 
	O.pkCryptoOrdem
	,O.dtCryptoOrdem
	,O.cdCryptoOrdemTaxaMoeda as cdCryptoMoeda
	, 'Venda - taxa'  AS  TipoOrdem
	, preco = Ct.VlClose
	, PT.DtCotacaoPtax
	, CotacaoDolar = pt.VlCotacaoPtaxCompra
	, Quantidade = O.vlCryptoOrdemTaxa
	, total = CONVERT(numeric(19,6),Ct.VlClose * O.vlCryptoOrdemTaxa)
	, corretagem = 0.000000
	, totalBrl = CONVERT(numeric(19,6),Ct.VlClose * O.vlCryptoOrdemTaxa) * pt.VlCotacaoPtaxCompra
	, corretagemBrl = 0.000000
	, O.cdCryptoOrdemHashArquivo
	from TblCryptoOrdem as O 
	left join TblCryptoCotacao as ct on ct.CdMoedaFrom = o.cdCryptoOrdemTaxaMoeda and CONVERT(date, ct.DtCotacao) = CONVERT(date, O.dtCryptoOrdem)
	left join TblCotacaoPtax as PT on CONVERT(date, PT.DtCotacaoPtax) = CONVERT(date, dbo.fcRetornarDataPtax(O.dtCryptoOrdem))
	WHERE O.cdCryptoOrdemMoedaVenda not in ('USD' , 'BRL')
	AND O.cdCryptoOrdemHashArquivo = @HashArquivo

END";


			migrationBuilder.Sql(view);
            migrationBuilder.Sql(spCarregarOrdem);
            migrationBuilder.Sql(spRemoverArquivo);
            migrationBuilder.Sql(alterVwApuracao);
			migrationBuilder.Sql(spRetornarApuracao);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tblCryptoExchange");

            migrationBuilder.DropTable(
                name: "tblTempCryptoOrdem");

            migrationBuilder.DropPrimaryKey(
                name: "PK_tblCryptoPar",
                table: "tblCryptoPar");

            migrationBuilder.DropColumn(
                name: "nmCryptoOrdemExchange",
                table: "TblCryptoOrdem");

            migrationBuilder.RenameTable(
                name: "tblCryptoPar",
                newName: "TblCryptoPar");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TblCryptoPar",
                table: "TblCryptoPar",
                column: "pkCryptoPar");
        }
    }
}
