﻿using Business.Models;
using Microsoft.EntityFrameworkCore;

namespace Business
{
    public class BdInvestManagerContext : DbContext
    {
        public BdInvestManagerContext(DbContextOptions<BdInvestManagerContext> options) : base(options) { }

        public DbSet<Ativo> TblAtivos { get; set; }
        public DbSet<TipoAtivo> TblTipoAtivo { get; set; }
        public DbSet<TblCryptoOrdem> TblCryptoOrdem { get; set; }
        public DbSet<tblCryptoMoeda> TblCryptoMoeda { get; set; }
        public DbSet<TblCryptoPar> TblCryptoPar { get; set; }
        public DbSet<tblCotacaoPtax> TblCotacaoPtax { get; set; }
        public DbSet<tblCryptoCotacao> TblCryptoCotacao { get; set; }
        public DbSet<VwCotacoesDesatulizadas> VwCotacoesDesatulizadas { get; set; }
        public DbSet<VwApuracaoCrypto> VwCryptoApuracao { get; set; }
        public DbSet<TblCryptoExchange> TblCryptoExchanges { get; set; }
        public DbSet<TblTempCryptoOrdem> TblTempCryptoOrdems { get; set; }
        public DbSet<VwExchangeParAtualizar> VwExchangeParAtualizar { get; set; }
        public DbSet<TblTempCryptoPar> TblTempCryptoPar { get; set; }
        public DbSet<TblTempCryptoCotacao> TblTempCryptoCotacao { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<VwCotacoesDesatulizadas>(d =>
            {
                d.HasNoKey();
                d.ToView("vwCotacoesDesatualizada");
            });
            modelBuilder.Entity<VwApuracaoCrypto>(d =>
            {
                d.HasNoKey();
                d.ToView("vwApuracao");
            });
            modelBuilder.Entity<VwExchangeParAtualizar>(d =>
            {
                d.HasNoKey();
                d.ToView("vwExchangeParAtualizar");
            });
        }
    }
}
