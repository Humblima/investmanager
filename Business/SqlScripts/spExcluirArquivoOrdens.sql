﻿CREATE PROCEDURE [dbo].[spExcluirArquivoOrdens]
	@dsHashArquivo varchar(200)
AS
	DELETE tblTempCryptoOrdem WHERE cdTempCryptoOrdemHashArquivo = @dsHashArquivo
	DELETE TblCryptoOrdem WHERE cdCryptoOrdemHashArquivo = @dsHashArquivo