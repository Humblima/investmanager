﻿CREATE FUNCTION [dbo].[fcRetornarDataPtax]
(
	@data date
)
RETURNS datetime
AS
BEGIN
	DECLARE @retorno datetime = (SELECT MAX(dtCotacaoPtax) from tblCotacaoPtax where CONVERT(date,dtCotacaoPtax) <= CONVERT(date,@data))
	
	Return @retorno
END