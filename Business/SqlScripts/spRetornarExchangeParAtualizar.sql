﻿CREATE PROCEDURE [dbo].[spRetornarExchangeParAtualizar]
	@cdHashArquivo varchar(200)
AS
BEGIN
	SELECT distinct
	t.nmTempCryptoOrdemExchange  as nmExchange
	FROM tblTempCryptoOrdem as t 
	left join tblCryptoPar as p on p.nmCryptoParExchange = t.nmTempCryptoOrdemExchange 
								AND CONCAT(P.cdCryptoParExchangeFrom, P.cdCryptoParExchangeTo) = T.cdTempCryptoOrdemPar 
    WHERE P.pkCryptoPar is null
	and T.cdTempCryptoOrdemHashArquivo = @cdHashArquivo
END
