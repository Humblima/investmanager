﻿CREATE VIEW [dbo].[vwCotacoesDesatualizada]
	AS 
SELECT 
cdMoeda
,MIN(dtOrdens) as dtMinOrdens
,MAX(dtOrdens) as dtMaxOrdens
FROM (
select 
O.cdCryptoOrdemMoedaCompra as cdMoeda
,CONVERT(date,O.dtCryptoOrdem) as dtOrdens
from TblCryptoOrdem as o
left join TblCryptoCotacao as C on C.CdMoedaFrom = O.cdCryptoOrdemMoedaCompra and CONVERT(date,O.dtCryptoOrdem) = CONVERT(date,C.DtCotacao)
where C.PkCotacao is null 
UNION
select 
O.cdCryptoOrdemMoedaVenda as cdMoeda
,CONVERT(date,O.dtCryptoOrdem) as dtMinOrdens
from TblCryptoOrdem as o
left join TblCryptoCotacao as C on C.CdMoedaFrom = O.cdCryptoOrdemMoedaVenda and CONVERT(date,O.dtCryptoOrdem) = CONVERT(date,C.DtCotacao)
where C.PkCotacao is null 
UNION
select 
O.cdCryptoOrdemTaxaMoeda as cdMoeda
,CONVERT(date,O.dtCryptoOrdem) as dtMinOrdens
from TblCryptoOrdem as o
left join TblCryptoCotacao as C on C.CdMoedaFrom = O.cdCryptoOrdemTaxaMoeda and CONVERT(date,O.dtCryptoOrdem) = CONVERT(date,C.DtCotacao)
where C.PkCotacao is null 
) as h
group by H.cdMoeda