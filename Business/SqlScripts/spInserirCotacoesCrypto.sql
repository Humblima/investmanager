﻿CREATE PROCEDURE [dbo].[spInserirCotacoesCrypto]
AS
BEGIN
	UPDATE C
	SET C.VlOpen = t.vlTempOpen
	, C.VlClose = t.vlTempClose
	, C.VlHigh = t.vlTempHigh
	, C.VlLow = t.vlTempLow
	FROM tblTempCryptoCotacao as T
	INNER join TblCryptoCotacao as c on c.CdMoedaFrom = t.cdTempMoedaFrom 
										AND C.CdMoedaTo = T.cdTempMoedaTo
										AND C.DtCotacao = t.dtTempCotacao
										AND (
											C.VlOpen <> t.vlTempOpen 
											OR C.VlHigh <> t.vlTempHigh
											OR C.VlClose <> t.vlTempClose
											OR C.VlLow <> t.vlTempLow 
											)

	INSERT INTO TblCryptoCotacao (
	CdMoedaFrom
	,CdMoedaTo
	,NrCotacaoTimeUnix
	,DtCotacao
	,VlHigh
	,VlLow
	,VlOpen
	,VlClose
	,DsCotacaoConversaoTipo
	,CdCotacaoConversaoFrom
	)
	SELECT 
	 T.cdTempMoedaFrom
	,T.cdTempMoedaTo
	,T.nrTempCotacaoTimeUnix
	,T.dtTempCotacao
	,T.vlTempHigh
	,T.vlTempLow
	,T.vlTempOpen
	,T.vlTempClose
	,T.dsTempCotacaoConversaoTipo
	,T.cdTempCotacaoConversaoFrom
	FROM tblTempCryptoCotacao as T
	LEFT join TblCryptoCotacao as c on c.CdMoedaFrom = t.cdTempMoedaFrom 
										AND C.CdMoedaTo = T.cdTempMoedaTo
										AND C.DtCotacao = t.dtTempCotacao
										AND C.VlOpen = t.vlTempOpen 
										AND C.VlHigh = t.vlTempHigh
										AND C.VlClose = t.vlTempClose
										AND C.VlLow = t.vlTempLow
	WHERE C.pkCotacao is null

	DELETE tblTempCryptoCotacao
											
END
