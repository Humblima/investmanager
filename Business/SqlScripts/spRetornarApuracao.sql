﻿CREATE PROCEDURE [dbo].[spRetornarApuracao]
	@HashArquivo varchar (200)
	
AS
BEGIN 

	SELECT 
	O.pkCryptoOrdem
	,O.dtCryptoOrdem
	,O.cdCryptoOrdemMoedaCompra as cdCryptoMoeda
	, CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN  'Compra' 
			WHEN O.dsCryptoOrdemLado = 'SELL' THEN 'Venda' END AS  TipoOrdem
	, precoUSD =CASE WHEN o.cdCryptoOrdemMoedaVenda = 'USD' THEN  O.vlCryptoOrdemPreco
					 WHEN o.cdCryptoOrdemMoedaVenda = 'BRL' THEN  CONVERT(numeric(19,6),O.vlCryptoOrdemPreco/CASe WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxCompra END)
					 ELSE CC.VlClose
					 END 
	, PT.DtCotacaoPtax
	, CotacaoDolar = CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxCompra END
	, Quantidade = O.nrCryptoOrdemQuantidade
	, totalUSD = CASE WHEN o.cdCryptoOrdemMoedaVenda = 'USD' THEN  O.vlCryptoOrdemTotal
					 WHEN o.cdCryptoOrdemMoedaVenda = 'BRL' THEN CONVERT(numeric(19,6),O.vlCryptoOrdemTotal/CASe WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxCompra END)
					 ELSE CONVERT(numeric(19,6),CC.VlClose * O.nrCryptoOrdemQuantidade)
					 END
	, corretagemUSD = CASE WHEN o.cdCryptoOrdemMoedaVenda = 'USD' THEN  o.vlCryptoOrdemTaxa
						   WHEN o.cdCryptoOrdemMoedaVenda = 'BRL' THEN CONVERT(numeric(19,6),o.vlCryptoOrdemTaxa/CASe WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxCompra END)
						   ELSE CONVERT(numeric(19,6),ct.VlClose * o.vlCryptoOrdemTaxa/2) 
					  END
	, TotalBRL =CASE WHEN o.cdCryptoOrdemMoedaVenda = 'USD' THEN CONVERT(numeric(19,6), o.vlCryptoOrdemTaxa * CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxCompra END)
						   WHEN o.cdCryptoOrdemMoedaVenda = 'BRL' THEN  O.vlCryptoOrdemTotal
						   ELSE CONVERT(numeric(19,6),CC.VlClose * O.nrCryptoOrdemQuantidade * CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxCompra END) 
						END
	, corretagemBRL = CASE WHEN o.cdCryptoOrdemTaxaMoeda = 'USD' THEN CONVERT(numeric(19,6),o.vlCryptoOrdemTaxa * CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxCompra END)
							WHEN o.cdCryptoOrdemTaxaMoeda = 'BRL' THEN o.vlCryptoOrdemTaxa
							ELSE CONVERT(numeric(19,6),(ct.VlClose * o.vlCryptoOrdemTaxa/2) * CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxVenda WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxCompra END)
						END
	, O.cdCryptoOrdemHashArquivo
	from TblCryptoOrdem as O 
	left join TblCryptoCotacao as CC on Cc.CdMoedaFrom = o.cdCryptoOrdemMoedaCompra and CONVERT(date, cC.DtCotacao) = CONVERT(date, O.dtCryptoOrdem)
	left join TblCryptoCotacao as ct on ct.CdMoedaFrom = o.cdCryptoOrdemTaxaMoeda and CONVERT(date, ct.DtCotacao) = CONVERT(date, O.dtCryptoOrdem)
	left join TblCotacaoPtax as PT on CONVERT(date, PT.DtCotacaoPtax) = CONVERT(date, dbo.fcRetornarDataPtax(O.dtCryptoOrdem))
	where  O.cdCryptoOrdemHashArquivo = @HashArquivo

	UNION ALL

	SELECT 
	O.pkCryptoOrdem
	,O.dtCryptoOrdem
	,O.cdCryptoOrdemMoedaVenda as cdCryptoMoeda
	, CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN 'Venda' 
			WHEN O.dsCryptoOrdemLado = 'SELL' THEN 'Compra'  END AS  TipoOrdem
	, preco = CV.VlClose
	, PT.DtCotacaoPtax
	, CotacaoDolar = CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxCompra WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxVenda END 
	, Quantidade = O.vlCryptoOrdemTotal
	, total = CONVERT(numeric(19,6),CV.VlClose * O.vlCryptoOrdemTotal)
	, corretagem = CONVERT(numeric(19,6),ct.VlClose * o.vlCryptoOrdemTaxa/2)
	, TotalBRL = CONVERT(numeric(19,6),CV.VlClose * O.vlCryptoOrdemTotal * CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxCompra WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxVenda END )
	, corretagemBRL = CONVERT(numeric(19,6),(ct.VlClose * o.vlCryptoOrdemTaxa/2) * CASe WHEN O.dsCryptoOrdemLado = 'BUY' THEN PT.VlCotacaoPtaxCompra WHEN O.dsCryptoOrdemLado = 'SELL' THEN PT.VlCotacaoPtaxVenda END )
	, O.cdCryptoOrdemHashArquivo
	from TblCryptoOrdem as O 
	left join TblCryptoCotacao as CV on CV.CdMoedaFrom = o.cdCryptoOrdemMoedaVenda and CONVERT(date, cV.DtCotacao) = CONVERT(date, O.dtCryptoOrdem)
	left join TblCryptoCotacao as ct on ct.CdMoedaFrom = o.cdCryptoOrdemTaxaMoeda and CONVERT(date, ct.DtCotacao) = CONVERT(date, O.dtCryptoOrdem)
	left join TblCotacaoPtax as PT on CONVERT(date, PT.DtCotacaoPtax) = CONVERT(date, dbo.fcRetornarDataPtax(O.dtCryptoOrdem))
	WHERE O.cdCryptoOrdemMoedaVenda not in ('USD' , 'BRL')
	AND O.cdCryptoOrdemHashArquivo = @HashArquivo

	UNION ALL

	 SELECT 
	O.pkCryptoOrdem
	,O.dtCryptoOrdem
	,O.cdCryptoOrdemTaxaMoeda as cdCryptoMoeda
	, 'Venda - taxa'  AS  TipoOrdem
	, preco = Ct.VlClose
	, PT.DtCotacaoPtax
	, CotacaoDolar = pt.VlCotacaoPtaxCompra
	, Quantidade = O.vlCryptoOrdemTaxa
	, total = CONVERT(numeric(19,6),Ct.VlClose * O.vlCryptoOrdemTaxa)
	, corretagem = 0.000000
	, totalBrl = CONVERT(numeric(19,6),Ct.VlClose * O.vlCryptoOrdemTaxa) * pt.VlCotacaoPtaxCompra
	, corretagemBrl = 0.000000
	, O.cdCryptoOrdemHashArquivo
	from TblCryptoOrdem as O 
	left join TblCryptoCotacao as ct on ct.CdMoedaFrom = o.cdCryptoOrdemTaxaMoeda and CONVERT(date, ct.DtCotacao) = CONVERT(date, O.dtCryptoOrdem)
	left join TblCotacaoPtax as PT on CONVERT(date, PT.DtCotacaoPtax) = CONVERT(date, dbo.fcRetornarDataPtax(O.dtCryptoOrdem))
	WHERE O.cdCryptoOrdemTaxaMoeda not in ('USD' , 'BRL')
	AND O.cdCryptoOrdemHashArquivo = @HashArquivo

END