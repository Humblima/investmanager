﻿CREATE PROCEDURE [dbo].[spCarregarCryptoOrdem]
	@HashArquivoOrdens varchar(200)
AS
BEGIN
	DECLARE @qntRegistros int

	INSERT INTO tblCryptoOrdem 
	(dtCryptoOrdem
	,cdCryptoOrdemPar
	,cdCryptoOrdemMoedaCompra
	,cdCryptoOrdemMoedaVenda
	,dsCryptoOrdemLado
	,vlCryptoOrdemPreco
	,nrCryptoOrdemQuantidade
	,vlCryptoOrdemTotal
	,vlCryptoOrdemTaxa
	,cdCryptoOrdemTaxaMoeda
	,cdCryptoOrdemHashArquivo
	,nmCryptoOrdemExchange
	)
	SELECT 
	 dtTempCryptoOrdem
	,cdTempCryptoOrdemPar
	,cdCryptoOrdemMoedaCompra = P.cdCryptoParExchangeFrom
	,cdCryptoOrdemMoedaVenda = P.cdCryptoParExchangeTo
	,dsTempCryptoOrdemLado
	,vlTempCryptoOrdemPreco
	,nrTempCryptoOrdemQuantidade
	,vlTempCryptoOrdemTotal
	,vlTempCryptoOrdemTaxa
	,cdTempCryptoOrdemTaxaMoeda
	,cdTempCryptoOrdemHashArquivo
	,nmTempCryptoOrdemExchange
	FROM tblTempCryptoOrdem as t
	inner join tblCryptoPar as p on p.nmCryptoParExchange = t.nmTempCryptoOrdemExchange 
								AND CONCAT(P.cdCryptoParExchangeFrom, P.cdCryptoParExchangeTo) = T.cdTempCryptoOrdemPar
	where T.cdTempCryptoOrdemHashArquivo = @HashArquivoOrdens

	
END