﻿CREATE PROCEDURE [dbo].[spInserirNovosParesExchange]
	@cdHashArquivo varchar(100)
AS
BEGIN
	INSERT INTO tblCryptoPar (
	nmCryptoParExchange, 
	cdCryptoParExchangeFrom,
	cdCryptoParExchangeTo,
	cdCryptoParFrom,
	cdCryptoParTo
	)
	SELECT 
	T.nmTempCryptoParExchange, 
	T.cdTempCryptoParExchangeFrom,
	T.cdTempCryptoParExchangeTo,
	T.cdTempCryptoParFrom,
	T.cdTempCryptoParTo
	FROM tblTempCryptoPar as T
	LEFT JOIN tblCryptoPar as P on P.nmCryptoParExchange = T.nmTempCryptoParExchange 
								AND P.cdCryptoParFrom = T.cdTempCryptoParFrom 
								AND P.cdCryptoParTo = T.cdTempCryptoParTo
	WHERE P.pkCryptoPar is null
	AND T.cdTempCryptoParHashArquivo = @cdHashArquivo
END
