using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InvestManager.ViewModels
{
  public class VmCryptoOrdemApurada
  {
    public DateTime dtCryptoOrdem { get; set; }
    public string cdCryptoMoeda { get; set; }
    public string dsCryptoTipoOrdem { get; set; }
    public decimal vlCryptoUSD { get; set; }
    public decimal nrCryptoQuantidade { get; set; }
    public decimal vlCryptoTotalUSD { get; set; }
    public decimal vlCryptoCorretagemUSD { get; set; }
    public DateTime dtCryptoCotacaoUSDBRL { get; set; }
    public decimal vlCryptoCotacaoUSDBRL { get; set; }
    public decimal vlCryptoTotalBRL { get; set; }
    public decimal vlCryptoCorretagemBRL { get; set; }

    public VmCryptoOrdemApurada() { }

    public VmCryptoOrdemApurada(Business.Models.VwApuracaoCrypto item)
    {
      this.cdCryptoMoeda = item.cdCryptoMoeda;
      this.dtCryptoOrdem = item.dtCryptoOrdem;
      this.dsCryptoTipoOrdem = item.TipoOrdem;
      this.vlCryptoUSD = item.precoUSD;
      this.nrCryptoQuantidade = item.quantidade;
      this.vlCryptoTotalUSD = item.totalUSD;
      this.vlCryptoCorretagemUSD = item.corretagemUSD;
      this.dtCryptoCotacaoUSDBRL = item.dtCotacaoPtax;
      this.vlCryptoCotacaoUSDBRL = item.CotacaoDolar;
      this.vlCryptoTotalBRL = item.totalBRL;
      this.vlCryptoCorretagemBRL = item.corretagemBRL;
    }

  }
}
