using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InvestManager.ViewModels
{
  public class VmRetornoListaOrdensCryptoApurada : ResultApi
  {
    public List<VmCryptoOrdemApurada> OrdensCrypto { get; set; }
  }
}
