using System.Collections.Generic;

namespace InvestManager.ViewModels
{
  public class VmRetornoListaExchange : ResultApi
  {
    public List<VmCriptoExchange> exchange { get; set; }

    public VmRetornoListaExchange ()
    {
      exchange = new List<VmCriptoExchange>();
    }
  }
}
