using Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InvestManager.ViewModels
{
  public class VmRetornarCarteiraBinance : ResultApi
  {
    public List<VmCryptoWalletValue> ListCryptoCarteira { get; set; }

    public VmRetornarCarteiraBinance()
    {
      this.ListCryptoCarteira = new List<VmCryptoWalletValue>();
    }

    public VmRetornarCarteiraBinance (List<CryptoCarteiraBinance> carteira)
    {
      ListCryptoCarteira = new List<VmCryptoWalletValue>();

      foreach (CryptoCarteiraBinance item in carteira)
      {
        VmCryptoWalletValue crypto = new VmCryptoWalletValue();
        crypto.CdCryptoMoeda = item.CdCryptoMoeda;
        crypto.vlQtdDisponivel = item.vlQtdDisponivel;
        crypto.vlQtdBloqueada = item.vlQtdBloqueada;
        ListCryptoCarteira.Add(crypto);
      }
    }
  }
}
