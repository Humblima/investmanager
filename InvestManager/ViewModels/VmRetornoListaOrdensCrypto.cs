using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InvestManager.ViewModels
{
  public class VmRetornoListaOrdensCrypto : ResultApi
  {
    public List<VmCriptoOrdem> OrdensCrypto { get; set; }
  }
}
