using System.Collections.Generic;

namespace InvestManager.ViewModels
{
  public class VmRetornoListaCotacaoDesatualizada : ResultApi
  {
    public List<VmCotacaoDesatualizada> ListCotacoesDesatualizadas { get; set; }
  }
}
