using Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InvestManager.ViewModels
{
  public class VmCryptoWalletValue
  {
    public string CdCryptoMoeda { get; set; }
    public decimal vlQtdDisponivel { get; set; }
    public decimal vlQtdBloqueada { get; set; }

  }
}
