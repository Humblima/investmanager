using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InvestManager.ViewModels
{
  public class VmCriptoOrdem
  {
    public int IdOrdem { get; set; }
    public DateTime dtOrdem { get; set; }
    public string cdPar { get; set; }
    public string cdMoedaCompra { get; set; }
    public string cdMoedaVenda { get; set; }
    public string dsLado { get; set; }
    public double vlPreco { get; set; }
    public double nrQuantidade { get; set; }
    public double vlTotal { get; set; }
    public double vlTaxa { get; set; }
    public string cdTaxaMoeda { get; set; }

    public VmCriptoOrdem() { }
    public VmCriptoOrdem(Business.Models.TblCryptoOrdem item)
    {

      IdOrdem = item.PkCryptoOrdem;
      dtOrdem = item.DtCryptoOrdem;
      cdPar = item.CdCryptoOrdemPar;
      cdMoedaCompra = item.CdCryptoOrdemMoedaCompra;
      cdMoedaVenda = item.CdCryptoOrdemMoedaVenda;
      dsLado = item.DsCryptoOrdemLado;
      vlPreco = item.VlCryptoOrdemPreco;
      nrQuantidade = item.NrCryptoOrdemQuantidade;
      vlTotal = item.VlCryptoOrdemTotal;
      vlTaxa = item.VlCryptoOrdemTaxa;
      cdTaxaMoeda = item.CdCryptoOrdemTaxaMoeda;
    }
  }
}
