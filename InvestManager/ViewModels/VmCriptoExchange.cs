using Business.Models;

namespace InvestManager.ViewModels
{
  public class VmCriptoExchange
  {
    public int IdCriptoExchange { get; set; }
    public string NmCriptoExchange { get; set; }
    public string CdCriptoExchange { get; set; }
    public string DsLinkImageExchange { get; set; }

    public VmCriptoExchange (TblCryptoExchange exchange)
    {
      this.IdCriptoExchange = exchange.IdCryptoExchange;
      this.NmCriptoExchange = exchange.NmCryptoExchange;
      this.CdCriptoExchange = exchange.CdCryptoExchange;
      this.DsLinkImageExchange = exchange.DsCryptoExchangeImagem;
    }
    public VmCriptoExchange() { }
  }
}
