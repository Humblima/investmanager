using Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InvestManager.ViewModels
{
  public class VmCotacaoDesatualizada
  {
    public string CdCryptoMoeda { get; set; }
    public DateTime DtCryptoAntigo { get; set; }
    public DateTime DtCryptoAtual { get; set; }

    public VmCotacaoDesatualizada(VwCotacoesDesatulizadas dadosVW)
    {
      CdCryptoMoeda = dadosVW.cdMoeda;
      DtCryptoAntigo = dadosVW.dtMinOrdens.Date;
      DtCryptoAtual = dadosVW.dtMaxOrdens.Date;
    }
  }
}
