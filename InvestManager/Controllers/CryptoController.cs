using Business;
using Business.Models;
using InvestManager.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InvestManager.Controllers
{
  [Route("api/Crypto")]
  [ApiController]
  public class CryptoController : ControllerBase
  {
    private readonly BdInvestManagerContext _context;



    public CryptoController(BdInvestManagerContext context)
    {
      _context = context;
    }

    [HttpGet]
    [Route("Teste")]
    public string Teste()
    {
      return "Ok Teste";
    }

    [HttpGet]
    [Route("AtualizarListaExchange")]
    public ActionResult<ResultApi> AtualizarListaExchange()
     {
      ResultApi result = new ResultApi();
      try
      {
        result.Message = "Foram atualizados " + Business.Models.TblCryptoExchange.AtualizarListaExchange(_context).ToString()
          + " Exchanges.";
      }
      catch (Exception ex)
      {
        result.Message = ex.Message;
      }
      return result;
     }
    [HttpGet]
    [Route("GetListaExchange")]
    public ActionResult<VmRetornoListaExchange> ListaExchange()
    {
      VmRetornoListaExchange result = new VmRetornoListaExchange();
      try
      {
        var lst = _context.TblCryptoExchanges.Select(e => e);
        foreach (var item in lst)
        {
          result.exchange.Add(new VmCriptoExchange(item));
        }
        result.Message = string.Concat(lst.Count().ToString(), " Exchanges");
      }
      catch (Exception ex)
      {
        result.exchange = new List<VmCriptoExchange>();
        result.Message = ex.Message;
      }
      return result;
    }
    [HttpPost]
    [Route("UploadFileText")]
    public ActionResult<VmRetornoListaOrdensCryptoApurada> UploadArquivoCrypto([FromBody] ArquivoOrdensCrypt file)
    {
      VmRetornoListaOrdensCryptoApurada result = new VmRetornoListaOrdensCryptoApurada
      {
        OrdensCrypto = new List<VmCryptoOrdemApurada>()
      };
      string hashArquivo = null;
      try
      {
        hashArquivo = Business.Models.TblTempCryptoOrdem.ImportarOrdensText(file.ConteudoArquivo, file.Exchange, _context).Result;

        List<VwApuracaoCrypto> lstApuracao = _context.VwCryptoApuracao.FromSqlRaw("EXEC spRetornarApuracao {0}", hashArquivo).ToList();

        foreach (VwApuracaoCrypto item in lstApuracao)
        {
          VmCryptoOrdemApurada ordem = new VmCryptoOrdemApurada(item);
          result.OrdensCrypto.Add(ordem);
        }
        result.Message = "Arquivo Processado.";
      }
      catch (Exception ex)
      {
        result.Message = ex.Message;
      }
      finally
      {
        if (hashArquivo != null)
        {
          _context.Database.ExecuteSqlRaw("EXEC spExcluirArquivoOrdens {0}", hashArquivo);
          _context.Database.CloseConnection();
        }
      }
      return result;
    }

    [HttpGet("CotacoesPendentes")]
    public VmRetornoListaCotacaoDesatualizada RetornarCotacoesPendentes()
    {
      VmRetornoListaCotacaoDesatualizada result = new VmRetornoListaCotacaoDesatualizada
      {
        ListCotacoesDesatualizadas = new List<VmCotacaoDesatualizada>()
      };
      try
      {
        var list = _context.VwCotacoesDesatulizadas.ToList();
        foreach (VwCotacoesDesatulizadas item in list)
        {
          result.ListCotacoesDesatualizadas.Add(new VmCotacaoDesatualizada(item));
        }
        result.Message = "Ok";
      }
      catch (Exception ex)
      {
        result.Message = ex.Message;
      }
      return result;
    }


    [HttpGet("WalletBinance")]
    public VmRetornarCarteiraBinance RetornarCarteiraBinance()
    {
      VmRetornarCarteiraBinance result = new VmRetornarCarteiraBinance();
      long timestap = DateTime.UtcNow.Ticks;
      
      try
      {
        CarteiraBinance carteira = new CarteiraBinance();
        result = new VmRetornarCarteiraBinance(carteira.RetornarPosicaoCarteira());
        result.Message = "Ok";
      }
      catch (Exception ex)
      {
        result.Message = ex.Message;
      }
      return result;
    }

  }
  public class ArquivoOrdensCrypt
  {
    public string NomeArquivo { get; set; }
    public string ConteudoArquivo { get; set; }
    public string Exchange { get; set; }
  }
}
