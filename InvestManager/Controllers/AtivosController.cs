using Business;
using Business.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InvestManager.Controllers
{
  [Route("api/Ativos")]
  [ApiController]
  public class AtivosController : ControllerBase
  {
    private readonly BdInvestManagerContext _context;

    public AtivosController(BdInvestManagerContext context)
    {
      _context = context;
    }

    // GET: api/Ativos
    [HttpGet]
    public async Task<ActionResult<IEnumerable<Ativo>>> GetTblAtivos()
    {
      return await _context.TblAtivos.ToListAsync();
    }

    // GET: api/Ativos/5
    [HttpGet("{id}")]
    public async Task<ActionResult<Ativo>> GetAtivo(int id)
    {
      var ativo = await _context.TblAtivos.FindAsync(id);

      if (ativo == null)
      {
        return NotFound();
      }

      return ativo;
    }
    // GET: api/Ativos/5
    [HttpGet("pesquisar/{pesquisa}")]
    public async Task<ActionResult<IEnumerable<string>>> GetListAtivos(string pesquisa)
    {
      var ativo = await _context.TblAtivos.Where<Ativo>(x => x.cdTicker.Contains(pesquisa)).Select(cd=>cd.cdTicker).ToListAsync();

      if (ativo == null)
      {
        return NotFound();
      }

      return ativo;
    }

    // PUT: api/Ativos/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see https://aka.ms/RazorPagesCRUD.
    [HttpPut("{id}")]
    public async Task<IActionResult> PutAtivo(int id, Ativo ativo)
    {
      if (id != ativo.pkAtivo)
      {
        return BadRequest();
      }

      _context.Entry(ativo).State = EntityState.Modified;

      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!AtivoExists(id))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return NoContent();
    }

    // POST: api/Ativos
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see https://aka.ms/RazorPagesCRUD.
    [HttpPost]
    public async Task<ActionResult<Ativo>> PostAtivo(Ativo ativo)
    {
      TipoAtivo tipoAtivo = _context.TblTipoAtivo.FirstOrDefault(ta => ta.dsTipoAtivo.Equals(ativo.TipoAtivo.dsTipoAtivo));
      if (tipoAtivo != null && tipoAtivo.pkTipoAtivo > 0)
      {
        ativo.TipoAtivo = tipoAtivo;
      }
      _context.TblAtivos.Add(ativo);

      
      await _context.SaveChangesAsync();

      return CreatedAtAction("GetAtivo", new { id = ativo.pkAtivo }, ativo);
    }

    // DELETE: api/Ativos/5
    [HttpDelete("{id}")]
    public async Task<ActionResult<Ativo>> DeleteAtivo(int id)
    {
      var ativo = await _context.TblAtivos.FindAsync(id);
      if (ativo == null)
      {
        return NotFound();
      }

      _context.TblAtivos.Remove(ativo);
      await _context.SaveChangesAsync();

      return ativo;
    }

    private bool AtivoExists(int id)
    {
      return _context.TblAtivos.Any(e => e.pkAtivo == id);
    }
  }
}
