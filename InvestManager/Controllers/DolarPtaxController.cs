using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business;
using InvestManager.ViewModels;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace InvestManager.Controllers
{
  [Route("api/CotacaoPTAX")]
  [ApiController]
  public class DolarPtaxController : ControllerBase
  {
    private readonly BdInvestManagerContext _context;

    public DolarPtaxController(BdInvestManagerContext context)
    {
      _context = context;
    }

    // GET: api/DolarPtax/AtualizarCotacao
    [HttpGet]
    [Route("AtualizarCotacao")]
    public ResultApi AtualizarCotacao(DateTime dtInicio, DateTime dtFim )
    {
      ResultApi result = new ResultApi();

      try
      {
        Business.Models.tblCotacaoPtax.AtualizarCotacaoPTAX(dtInicio, dtFim, _context);
        result.Message = "Cotação PTAX Atualizada.";
      }
      catch (Exception ex)
      {
        result.Message = ex.Message;
      }
      return result;
    }
  }
}
