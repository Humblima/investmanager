export class CryptoOrdem {

  public DataOrdem: string;
  public ParOrdem: string;
  public TypeOrdem: string;
  public PriceOrdem: number;
  public AmountOrdem: number;
  public TotalOrdem: number;
  public FeeOrdem: number;
  public FeeCoinOrdem: string;

}
