import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SimularNotaComponent } from './simulador-notas/simular-nota/simular-nota.component';
import { CryptosComponent } from './cryptos/cryptos.component';


const routes: Routes = [
  { path: '', component: CryptosComponent, pathMatch: 'full' },
  { path: 'cryptos', component: CryptosComponent },
  { path: 'simularnota', component: SimularNotaComponent },
    //{ path: 'fetch-data', component: FetchDataComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
