import { Injectable } from '@angular/core';
import { CryptoOrdem } from './crypto-ordem';
import { ExportToCsv } from 'export-to-csv';
import { OrdemApuradaVm } from './cryptos/ordem-apurada-vm';

@Injectable({
  providedIn: 'root'
})
export class CsvReadService {

  constructor() {
    this.headerOficial = 'Date(UTC);Market;Type;Price;Amount;Total;Fee;Fee Coin'.split(';');
  }

  public headerOficial: string[];

  isValidCSVFile(fileName: string, content:string) {

    let isValid = false;

    isValid = fileName.endsWith(".csv");

    if (isValid == true) {
    
      let headersRow = this.getHeaderArray(this.getLinhasArquivo(content));
        let cont: number = 0;

        if (headersRow.length != this.headerOficial.length)
          isValid = false;

        while (isValid == true && cont < headersRow.length) {

          if (headersRow[cont].trim() != this.headerOficial[cont].trim()) {
            isValid = false;
          }
          cont = cont + 1;
        }
    }

    return isValid;
  }

  getLinhasArquivo (content: string): string[]{
    
    return content.split(/\r\n|\n/);
  }

  getHeaderArray(csvRecordsArr: any) {
    let headers = (<string>csvRecordsArr[0]).split(';');
    let headerArray = [];
    for (let j = 0; j < headers.length; j++) {
      headerArray.push(headers[j]);
    }
    return headerArray;
  }

  getCryptoOrdemFromCSV(csvRecordsArray: any) {
    let csvArr: CryptoOrdem[] = [];
    let cabecalho = [];
    for (let i = 0; i < csvRecordsArray.length; i++) {

      if (i = 0) {
        cabecalho = (<string>csvRecordsArray[i]).split(';');
      }
      else {
        let linhaAtual = (<string>csvRecordsArray[i]).split(';');
        if (linhaAtual.length == cabecalho.length) {
          let ordemCrypto: CryptoOrdem = new CryptoOrdem();
          for (let j = 0; j < linhaAtual.length; j++) {
            switch (linhaAtual[j].trim()) {
              case 'Date(UTC)': {
                ordemCrypto.DataOrdem = linhaAtual[j].trim();
                break;
              }
              case 'Market': {
                ordemCrypto.ParOrdem = linhaAtual[j].trim();
                break;
              }
              case 'Type': {
                ordemCrypto.TypeOrdem = linhaAtual[j].trim();
                break;
              }
              case 'Price': {
                ordemCrypto.PriceOrdem = Number(linhaAtual[j].trim());
                break;
              }
              case 'Amount': {
                ordemCrypto.AmountOrdem = Number(linhaAtual[j].trim());
                break;
              }
              case 'Total': {
                ordemCrypto.TotalOrdem = Number(linhaAtual[j].trim());
                break;
              }
              case 'Fee': {
                ordemCrypto.FeeOrdem = Number(linhaAtual[j].trim());
                break;
              }
              case 'Fee Coin': {
                ordemCrypto.FeeOrdem = Number(linhaAtual[j].trim());
                break;
              }
              default:
                break;
            }
          }
          csvArr.push(ordemCrypto);
        }
      }

    }
    return csvArr;
  }
  downloadCsvFile(listOrdensCrypto: OrdemApuradaVm[]) {
    const options = {
      fieldSeparator: ';',
      filename: 'OrdensCrypto-Resultado',
      quoteStrings: '"',
      decimalSeparator: ',',
      showLabels: true,
      showTitle: false,
      title: 'OrdensApuradas',
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: false,
      headers: [
      'Data(UTC)' 
      ,'Moeda'
      ,'Ordem'
      ,'Cotação USD'
      ,'Quantidade'
      ,'Total USD'
      ,'Corretagem USD'
      ,'Data Cotacao Dolar'
      ,'Valor Cotacao Dolar'
      ,'Total BRL'
      ,'Corretagem BRL'
      ]
      // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
    };

    const csvExporter = new ExportToCsv(options);

    csvExporter.generateCsv(listOrdensCrypto);
  }
}

