import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CryptosComponent } from './cryptos.component';

describe('CryptosComponent', () => {
  let component: CryptosComponent;
  let fixture: ComponentFixture<CryptosComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CryptosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CryptosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
