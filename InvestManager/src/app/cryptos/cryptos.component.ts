import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';
import { OrdemApuradaVm } from './ordem-apurada-vm';
import { CsvReadService } from '../csv-read.service';
import { ArquivoOrdemVm } from './arquivo-ordem-vm';
import { NgxSpinnerService } from "ngx-spinner";
import { ToastService } from '../toast.service';
import { Exchange } from './exchange';


@Component({
  selector: 'app-cryptos',
  templateUrl: './cryptos.component.html',
  styleUrls: ['./cryptos.component.scss']
})
export class CryptosComponent implements OnInit {
  selectedFile: FileList = null;
  private arquivoOrdem: ArquivoOrdemVm = new ArquivoOrdemVm();
  public fileContent: string = '';
  public mensagemResultado: string = '';
  public lstExchanges: Exchange[] = [];
  public listOrdensCrypto: OrdemApuradaVm[] = [];
  public exchangeSelecionada: Exchange;

  @ViewChild('cryptoArquivo') cryptoArquivo: any;

  constructor(private http: HttpClient
    , private csvReader: CsvReadService
    , private spinner: NgxSpinnerService
    , public toastService: ToastService
  ) { }

  ngOnInit(): void {
    this.http.get<any>('/api/Crypto/GetListaExchange').subscribe(data => {
      this.lstExchanges = data.exchange;
    });
  }

  search = (text$: Observable<string>) => text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    filter(term => term.length >= 1),
    map(term => this.lstExchanges.filter(exchange => new RegExp(term, 'mi').test(exchange.nmCriptoExchange)).slice(0, 10))
  )
  formatter = (exchange: Exchange) => exchange.nmCriptoExchange;

  selecionarArquivo(event) {
    this.selectedFile = <FileList>event.srcElement.files;
    let reader = new FileReader();
    reader.readAsText(this.selectedFile[0]);
    reader.onload = () => {
      let csvData = reader.result;
      this.fileContent = (<string>csvData);
    };
    
  }

  exportarCSV() {
    this.csvReader.downloadCsvFile(this.listOrdensCrypto);
  }

  enviarArquivo() {
    this.spinner.show();
    let validFile = this.csvReader.isValidCSVFile(this.selectedFile[0].name, this.fileContent);

    if (validFile == true) {
      this.arquivoOrdem.NomeArquivo = this.selectedFile[0].name;
      this.arquivoOrdem.ConteudoArquivo = this.fileContent;
      this.arquivoOrdem.Exchange = this.exchangeSelecionada.cdCriptoExchange;
      this.http.post<any>('/api/Crypto/UploadFileText', this.arquivoOrdem)
        .subscribe(result => {
          this.mensagemResultado = result.message;
          this.listOrdensCrypto = result.ordensCrypto.sort((a, b) => (a.dtCryptoOrdem < b.dtCryptoOrdem) ? -1 : 1);
          this.spinner.hide();
          this.toastService.show(this.mensagemResultado, { classname: 'bg-success text-light', delay: 10000 });
        }
        );
    }
    else {
      this.spinner.hide();
      this.toastService.show('Arquivo com formato inválido, revise o seu arquivo.', { classname: 'bg-danger text-light', delay: 10000 });
    }
  }
}
