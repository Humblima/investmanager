export class OrdemApuradaVm {
  public dtCryptoOrdem: Date;
  public cdCryptoMoeda: string;
  public dsCryptoTipoOrdem: string;
  public vlCryptoUSD: number;
  public nrCryptoQuantidade: number;
  public vlCryptoTotalUSD: number;
  public vlCryptoCorretagemUSD: number;
  public dtCryptoCotacaoUSDBRL: Date;
  public vlCryptoCotacaoUSDBRL: number;
  public vlCryptoTotalBRL: number;
  public vlCryptoCorretagemBRL: number;
}
