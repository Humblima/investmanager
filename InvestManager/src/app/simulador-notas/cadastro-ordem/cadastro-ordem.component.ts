import { Component, OnInit } from '@angular/core';
import { Ordem } from '../ordem';
import { NotaCorretagemService } from '../nota-corretagem.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { AtivosService } from '../../ativos.service';

@Component({
  selector: 'app-cadastro-ordem',
  templateUrl: './cadastro-ordem.component.html',
  styleUrls: ['./cadastro-ordem.component.scss']
})
export class CadastroOrdemComponent implements OnInit {

  ordemNova: Ordem = new Ordem();
  selectedCVCodigo: string = 'C';
  closeResult: string;

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(term => term.length < 2 ? []
        : this.ativosService.getPesquisarCdAtivo(term))
    );

  constructor(public notaCorretagem: NotaCorretagemService
    , private modalService: NgbModal
    , public ativosService: AtivosService) {

  }

  ngOnInit(): void {
  }
  onChange(value) {
    this.ordemNova.cdCompraVenda = value;
  }

  salvarOrdem() {
    this.notaCorretagem.adicionarOrdem(this.ordemNova);
    let cdOrdemSelecionado = this.ordemNova.cdCompraVenda;
    this.ordemNova = new Ordem();
    this.ordemNova.cdCompraVenda = cdOrdemSelecionado;
    this.modalService.dismissAll('Save click');

  }
  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
