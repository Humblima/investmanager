import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CadastroOrdemComponent } from './cadastro-ordem.component';

describe('CadastroOrdemComponent', () => {
  let component: CadastroOrdemComponent;
  let fixture: ComponentFixture<CadastroOrdemComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroOrdemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroOrdemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
