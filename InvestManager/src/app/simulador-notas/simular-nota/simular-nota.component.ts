import { Component, OnInit } from '@angular/core';
import { NotaCorretagemService } from '../nota-corretagem.service';


@Component({
  selector: 'app-simular-nota',
  templateUrl: './simular-nota.component.html',
  styleUrls: ['./simular-nota.component.scss']
})
export class SimularNotaComponent implements OnInit {

  constructor(public notaCorretagem: NotaCorretagemService) {

  }

  ngOnInit(): void {
  }

  retornarValortotal() {
    return this.notaCorretagem.retornarValorTotalOperacoes();
  }

  
}
