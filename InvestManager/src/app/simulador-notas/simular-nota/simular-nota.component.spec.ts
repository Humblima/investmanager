import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SimularNotaComponent } from './simular-nota.component';

describe('SimularNotaComponent', () => {
  let component: SimularNotaComponent;
  let fixture: ComponentFixture<SimularNotaComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SimularNotaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimularNotaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
