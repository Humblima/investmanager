import { Component, OnInit } from '@angular/core';
import { Ordem } from '../ordem';
import { NotaCorretagemService } from '../nota-corretagem.service';

@Component({
  selector: 'app-ordem-executada',
  templateUrl: './ordem-executada.component.html',
  styleUrls: ['./ordem-executada.component.scss']
})
export class OrdemExecutadaComponent implements OnInit {

 
   constructor(public notaCorretagem: NotaCorretagemService) { }

  ngOnInit(): void {
  }

  excluirOrdem(objeto: Ordem) {
    this.notaCorretagem.removerOrdem(objeto);
  }
  valorTotalOperacao() {
    return this.notaCorretagem.retornarValorTotalOperacoes();
  }
  cdDebitoCreditoOperacaoFinal() {
    return this.notaCorretagem.cdDebitoCreditoOperacaoFinal();
  }
  listarOrdensCadastradas() {
    return this.notaCorretagem.listaOrdens;
  }
}
