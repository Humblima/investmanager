import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OrdemExecutadaComponent } from './ordem-executada.component';

describe('OrdemExecutadaComponent', () => {
  let component: OrdemExecutadaComponent;
  let fixture: ComponentFixture<OrdemExecutadaComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdemExecutadaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdemExecutadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
