import { Injectable } from '@angular/core';
import { Ordem } from './ordem';


@Injectable({
  providedIn: 'root'
})
export class NotaCorretagemService {
  private _dataNota: Date;

  public set dataNota(v: Date) {
    this._dataNota = v;
  }

  public get dataNota(): Date {
    return this._dataNota;
  }

  private _nmCorretora: string;

  public set nmCorretora(v: string) {
    this._nmCorretora = v;
  }

  public get nmCorretora(): string {
    return this._nmCorretora;
  }


  private _lstOrdens: Array<Ordem>;
  public get listaOrdens(): Array<Ordem> {
    return this._lstOrdens;
  }

  constructor() {
    this._lstOrdens = new Array<Ordem>();
    this._dataNota = new Date();
  }

  public adicionarOrdem(ordemNova: Ordem) {
    this._lstOrdens.push(ordemNova);
  }

  public removerOrdem(ordemExcluir: Ordem) {
    let indice: number = this._lstOrdens.indexOf(ordemExcluir);
    this._lstOrdens.splice(indice, 1);
  }

  public retornarValorTotalOperacoes() {
    return this._lstOrdens.reduce((sum, oremAtual) => sum + oremAtual.getValorOperacaoBrutoTotalizador(), 0);
  }
  public retornarValorTotalTaxaLiquidacao() {
    return this._lstOrdens.reduce((sum, oremAtual) => sum + oremAtual.vlTxLiquidacao, 0);
  }
  public retornarValorTotalTaxaRegistro() {
    return this._lstOrdens.reduce((sum, oremAtual) => sum + oremAtual.vlTxRegistro, 0);
  }
  public retornarValorTotalEmolumentos() {
    return this._lstOrdens.reduce((sum, oremAtual) => sum + oremAtual.vlTxNegociacao, 0);
  }
  public retornarValorTotalCBLC(): number{
    return (this.retornarValorTotalOperacoes() +
      this.retornarValorTotalTaxaLiquidacao() +
      this.retornarValorTotalTaxaRegistro());
  }
  public retornarCdDebitoCreditoTotalCBLC(): string {
    return this.retornarValorTotalCBLC() >=0 ? "C": "D";
  }
  public retornarValorTotalBovespa() {
    return this.retornarValorTotalEmolumentos();
  }

  public retornarValorLiquidoNota() {
    return this._lstOrdens.reduce((sum, oremAtual) => sum + oremAtual.vlOperacaoLiquido, 0);
  }
  public retornarValorLiquidoNotaAbs() {
    return Math.abs(this.retornarValorLiquidoNota());
  }
  public retornarCdDebitoCreditoLiquidoNota() {
    return this.retornarValorLiquidoNota() >= 0.00 ? "C" : "D";
  }


  public retornarValorVendaBruto() {
    let total: number = 0.00;
    this._lstOrdens.forEach(element => {
      if (element.cdCompraVenda === 'V') {
        total = total + element.vlOperacao;
      }
    });
    return total;
  }

  public retornarValorTotalIRRF() {
    let total: number = 0.00;
    this._lstOrdens.forEach(element => {
      if (element.cdCompraVenda === 'V') {
        total = total + element.vlIRRF;
      }
    });
    return total;
  }

  public ValorAbsoluto(v: number): number{
    return Math.abs(v);
  }

  public cdDebitoCreditoOperacaoFinal() {
    return Ordem.getCdDebitoCreditoValor(this._lstOrdens.reduce((sum, oremAtual) => sum + oremAtual.getValorOperacaoBrutoTotalizador(), 0));
  }

}
