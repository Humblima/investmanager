import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ResultadoNotaComponent } from './resultado-nota.component';

describe('ResultadoNotaComponent', () => {
  let component: ResultadoNotaComponent;
  let fixture: ComponentFixture<ResultadoNotaComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultadoNotaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultadoNotaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
