import { Component, OnInit } from '@angular/core';
import { NotaCorretagemService } from '../nota-corretagem.service';
import { Ordem } from '../ordem';


@Component({
  selector: 'app-resultado-nota',
  templateUrl: './resultado-nota.component.html',
  styleUrls: ['./resultado-nota.component.scss']
})
export class ResultadoNotaComponent implements OnInit {

  constructor(public nota: NotaCorretagemService) { }

  ngOnInit(): void {
  }
  getListaOrdens(): Ordem[] {
    return this.nota.listaOrdens;
  }
}
