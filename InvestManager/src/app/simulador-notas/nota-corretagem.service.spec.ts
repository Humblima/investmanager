import { TestBed } from '@angular/core/testing';

import { NotaCorretagemService } from './nota-corretagem.service';

describe('NotaCorretagemService', () => {
  let service: NotaCorretagemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NotaCorretagemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
