export class Ordem {

  private _cdCompraVenda: string;
  public set cdCompraVenda(v: string) {
    this._cdCompraVenda = v;
  }
  public get cdCompraVenda(): string {
    return this._cdCompraVenda;
  }
  public get cdDebitoCredito(): string {
    if (this._cdCompraVenda === 'C') {
      return 'D'
    }
    else if (this._cdCompraVenda === 'V') {
      return 'C'
    }
    else {
      return '';
    }
  }

  
  private _cdAtivo: string;
  public set cdAtivo(v: string) {
    this._cdAtivo = v;
  }
  public get cdAtivo(): string {
    return this._cdAtivo;
  }

  private _isDayTrade: boolean = false;
  public set isDayTrade(v: boolean) {
    this._isDayTrade = v;
  }
  public get isDayTrade(): boolean {
    return this._isDayTrade;
  }
  
  private _nrQuantidade: number;
  public set nrQuantidade(v: number) {
    this._nrQuantidade = v;
  }
  public get nrQuantidade(): number {
    return this._nrQuantidade;
  }

  private _vlPrecoAjuste: number;
  public set vlPrecoAjuste(v: number) {
    this._vlPrecoAjuste = v;
  }
  public get vlPrecoAjuste(): number {
    return this._vlPrecoAjuste;
  }

  private _vlTxCorretagem: number = 0.00;
  public set vlTxCorretagem(v: number) {
    this._vlTxCorretagem = v;
  }
  public get vlTxCorretagem(): number {
    return this._vlTxCorretagem;
  }

  public get vlOperacao(): number {
    return this._vlPrecoAjuste * this._nrQuantidade * (this._cdCompraVenda === 'C' ? -1 : 1);
  }
  public get vlOperacaoAbs(): number {
    return Math.abs(this.vlOperacao);
  }

  public get vlTxNegociacao(): number {
    return Ordem.truncate(this.vlOperacaoAbs * (0.003802 / 100.00), 2) * -1;
  }
  public get vlTxLiquidacao(): number {
    return Ordem.truncate(this.vlOperacaoAbs * (0.0275 / 100.00), 2) * -1;
  }
  public get vlTxRegistro(): number {
    return 0.00;
  }
  public get vlIRRF(): number {
    return this._cdCompraVenda === 'V' ? Ordem.truncate(this.vlOperacaoAbs * (0.005 / 100.00), 2) : 0.00 * -1;
  }

  public get vlOperacaoLiquido(): number {
    return this.vlOperacao + this.vlTxNegociacao + this.vlTxLiquidacao + this.vlTxRegistro ;
  }
  public get vlOperacaoLiquidoAbs(): number {
    return Math.abs(this.vlOperacaoLiquido);
  }

  constructor() {
    this.cdCompraVenda = 'C';
  }

  getValorTaxas(): number {
    return (this.vlTxNegociacao + this.vlTxLiquidacao + this.vlTxRegistro);
  }
  getValorTaxasAbs() {
    return Math.abs(this.getValorTaxas());
  }

  getValorOperacaoBruto() {
    return this.vlOperacao;
  }
  getValorOperacaoBrutoTotalizador() {
    return this.vlOperacao ;
  }

  static getCdDebitoCreditoValor(valor: number) {
    if (valor < 0) {
      return 'D'
    }
    else {
      return 'C'
    }
  }
  getQtVisualizacaoFinal() {
    if (this.cdCompraVenda === 'C') {
      return Math.abs(this.nrQuantidade);
    }
    else if (this.cdCompraVenda === 'V') {
      return Math.abs(this.nrQuantidade) * (-1);
    }
  }
  static truncate(num: number, places: number) {
    return num; //Math.trunc(num * Math.pow(10, places)) / Math.pow(10, places);
  }
}
