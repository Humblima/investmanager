import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NotaCorretagemService } from './nota-corretagem.service';
import { AtivosService } from "../ativos.service";
import { SimularNotaComponent } from './simular-nota/simular-nota.component';
import { OrdemExecutadaComponent } from './ordem-executada/ordem-executada.component';
import { ResultadoFinanceiroComponent } from './resultado-financeiro/resultado-financeiro.component';
import { ResultadoNotaComponent } from './resultado-nota/resultado-nota.component';
import { CadastroOrdemComponent } from './cadastro-ordem/cadastro-ordem.component';



@NgModule({
  declarations: [
    SimularNotaComponent,
    OrdemExecutadaComponent,
    ResultadoFinanceiroComponent,
    ResultadoNotaComponent,
    CadastroOrdemComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule
  ],
  providers: [
    NotaCorretagemService,
    AtivosService
  ]
})
export class SimuladorNotasModule { }
