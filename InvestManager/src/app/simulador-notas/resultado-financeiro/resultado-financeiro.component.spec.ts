import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ResultadoFinanceiroComponent } from './resultado-financeiro.component';

describe('ResultadoFinanceiroComponent', () => {
  let component: ResultadoFinanceiroComponent;
  let fixture: ComponentFixture<ResultadoFinanceiroComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultadoFinanceiroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultadoFinanceiroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
