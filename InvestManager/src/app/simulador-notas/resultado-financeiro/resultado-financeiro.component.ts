import { Component, OnInit } from '@angular/core';
import { NotaCorretagemService } from '../nota-corretagem.service'

@Component({
  selector: 'app-resultado-financeiro',
  templateUrl: './resultado-financeiro.component.html',
  styleUrls: ['./resultado-financeiro.component.scss']
})
export class ResultadoFinanceiroComponent implements OnInit {

  constructor(public notaCorretagem: NotaCorretagemService) { }

  ngOnInit(): void {
  }

}
