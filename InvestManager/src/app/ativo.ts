import { TipoAtivo } from './tipo-ativo';

export class Ativo {
  public pkAtivo: number; 
  public cdTicker: string;
  public nmAtivo: string;
  public TipoAtivo: TipoAtivo;
}
