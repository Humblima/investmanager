import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { NgModule, LOCALE_ID, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
registerLocaleData(localePt);

import { SimuladorNotasModule } from './simulador-notas/simulador-notas.module';
import { AtivosService } from './ativos.service';
import { UploadService } from './upload.service';
import { CsvReadService } from './csv-read.service';
import { ToastService } from './toast.service'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule} from '@angular/common/http';
import { CryptosComponent } from './cryptos/cryptos.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { ToastContainerComponent } from './toast-container/toast-container.component';




@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    CryptosComponent,
    ToastContainerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    SimuladorNotasModule,
    NgbModule,
    HttpClientModule,
    NgxSpinnerModule
   ],
  providers: [
    { provide: LOCALE_ID, useValue: 'pt' },
    AtivosService,
    UploadService,
    CsvReadService,
    ToastService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
