import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Ativo } from './ativo';

@Injectable()
export class AtivosService {

  private ativosUrl: string = 'api/Ativos';

  constructor(private http: HttpClient) { }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    
  }

  getAtivos(): Observable<Ativo[]> {
    return this.http.get<Ativo[]>(this.ativosUrl);
  }
  /** GET hero by id. Will 404 if id not found */
  getPesquisarCdAtivo(pesquisa: string): Observable<string[]> {
    const url = `${this.ativosUrl}/pesquisar/${pesquisa}`;
    return this.http.get<string[]>(url);
  }

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
