import { TestBed } from '@angular/core/testing';

import { CsvReadService } from './csv-read.service';

describe('CsvReadService', () => {
  let service: CsvReadService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CsvReadService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
